import glob
import os
import psutil

pids = []
pexe = []
times = []
# store pid information
for proc in psutil.process_iter():
    path = proc.cmdline()
    if "test" in " ".join(path):
        pexe.append(proc.exe())
        pids.append(proc.pid)
        times.append(proc.cpu_times().user)

def tabrow(args, maxlen=20):
    if type(maxlen) == list:
        maxlist = maxlen + [maxlen[-1]]*(len(args) - len(maxlen))
    else:
        maxlist = [maxlen]*len(args)
    return "".join([str(x) + " "*(maxlist[ii] - len(str(x))) for ii, x in enumerate(args)])

tabline = "-"*100
cols = [30,10]

print tabline
print tabrow(["test", "status", "time/min", "pid", "%", "left/h", "last line"], maxlen=cols)
print tabline
nrun = 0
for ff in sorted(glob.glob("test_*")):
    fname = ff + "/output"
    status = "?"
    time = pid = expect = percent = "-"
    last_line = "[no last output line]"
    if os.path.exists(fname):
        with open(fname,'r') as f:
                output = [x for x in f.read().split("\n") if x.strip()!=""]
        last_line = output[-1].strip()
        if last_line == "done!":
            status = ">DONE<"
        else:
            status = "problems?"
        run = False
        for i, ee in enumerate(pexe):
            if ff + "/" in ee:
                run = True
                time = str(round(times[i]/60.,1))
                pid = str(pids[i])
        if run:
            status = "run"
            nrun += 1
            percent = float(last_line.split(" ")[-1])
            if percent > 0e0:
                expect = round((float(time) / percent - float(time))/60.,1)
    else:
        status = "no output"
    print tabrow([ff, status, time, pid, percent, expect, last_line], maxlen=cols)
print tabline
print tabrow(["running", "", "", nrun], maxlen=cols)
print tabline

