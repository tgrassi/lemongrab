import sys
# move to the python source folder
sys.path.insert(0, "./src_py/")

import shutil
import os
from network import Network


# *****************
# return a list with the models
def get_models_list(model_folder="models/"):
    folders = os.walk(model_folder)
    return [x[0].replace(model_folder, "") for x in folders if x[0] != model_folder]


# ***************
def get_options(folder):
    data_options = dict()
    fname = folder + "/options.opt"

    # check if option file exists
    if not os.path.isfile(fname):
        print("ERROR: missing " + fname)
        sys.exit()

    # loop to store options in the dictionary
    for row in open(fname):
        srow = row.strip()
        if srow == "":
            continue
        if srow.startswith("#"):
            continue
        k, v = [x.strip() for x in srow.split(":")]
        data_options[k] = v

    return data_options


# ***************
# copy model files to main folder
def make_model(name):

    # get folders starting with name
    models_match = []
    for model in get_models_list():
        if name in model:
            models_match.append(model)

    # check if model folder exists
    if not models_match:
        print("ERROR: model " + name + " does not exist!")
        print(" Available models are:")
        print(get_models_list())
        sys.exit()

    if len(models_match) > 1:
        print("ERROR: multiple matches for " + name)
        print(" Choose one among these:")
        print(models_match)
        sys.exit()

    # model folder
    source_folder = "models/" + models_match[0]

    # copy files
    for filename in ["input.dat", "input_chemistry.dat", "test.f90"]:
        print("copying " + source_folder + "/" + filename + " from model " + models_match[0])
        if os.path.isfile(filename):
            shutil.copy(filename, filename + ".bak")
        shutil.copy(source_folder + "/" + filename, ".")

    return get_options(source_folder)


if __name__ == "__main__":

    # check usage
    if len(sys.argv) != 2:
        print("ERROR: usage is")
        print(" python " + sys.argv[0] + " model")
        print("where available models are:")
        print(get_models_list())
        sys.exit()

    # copy files for the given model, and get options
    options = make_model(sys.argv[1])

    # load network
    ntw = Network(options)

    # preprocess data
    ntw.preproc()
