import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os
from tqdm import tqdm

font = {'family': 'sans',
        'weight': 'normal',
        'size': 14}

matplotlib.rc('font', **font)

folder = "movie/"

if not os.path.exists(folder):
    os.makedirs(folder)

# reactions verbatim
verbs = ["H2 -> Mg+ + e-", "Mg+ + e- -> H2", "g-- + g -> g- + g-", "g-- + g+ -> g + g-",
         "g-- + g++ -> g + g", "g- + g+ -> g + g", "g- + g++ -> g + g+", "g++ + g -> g+ + g+",
         "Mg+ + g-- -> H2 + g-", "Mg+ + g- -> H2 + g", "Mg+ + g -> H2 + g+", "Mg+ + g+ -> H2 + g++",
         "e- + g- -> g--", "e- + g -> g-", "e- + g+ -> g", "e- + g++ -> g+"]

# grain variables
var_dust = ["g" + str(i) for i in range(-2,3)]

# ions variables
var_ions = ["Mg+", "e-"] + [x for x in var_dust if x != "g0"]

# rho*beta for MHD
betas = ["(rho_" + x + ")*beta_" + x for x in var_ions]

# resistivity coefficients
etas = ["eAD", "eOhm", "eHall"] + ["eAD_tab", "eOhm_tab", "eHall_tab"]

# all hydro variables
var_names_hydro = ["xpos", "time", "Tgas", "rho", "rvx", "rvy", "rvz", "By", "Bz",
                   "energy", "zeta", "H2", "Mg+", "e-"] + var_dust \
                  + ["zeta_N", "vx", "vy", "vz", "|B|"] \
                  + etas + ["x_H2", "x_Mg+", "x_e-"] + ["x_" + x for x in var_dust] \
                  + ["cool", "heat", "ctime"] + betas + verbs \
                  + ["g_acc", "g_acc x rho", "E_K", "E_B"]

log_scale = ["rho"]


# sanitize filename
def slugify(value):
    import unicodedata
    import re
    value = value.decode('unicode-escape')
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = unicode(re.sub('[^\w\s-]', '', value).strip().lower())
    value = unicode(re.sub('[-\s]+', '-', value))
    return value


# load files and store into dict, keys=variables, value=time evolution
def load(var_names, fname):
    print "loading " + fname
    data = {x: [] for x in var_names}
    for row in open(fname, "rb"):
        srow = row.strip()
        if srow == "":
            continue
        arow = [float(x) for x in srow.split(" ") if x != ""]
        for ii, v in enumerate(var_names):
            data[v].append(arow[ii])
    data = {k: np.array(v) for k, v in data.iteritems()}
    data["fname"] = data["label"] =\
        fname.replace(".dat", "").replace("/output_hydro", "").replace("test_", "")
    return data


# find a data subset for key1 where key2 is equal to value
def subset(data, key1, key2, value):
    subdata = []
    for ii, vv in enumerate(data[key2]):
        if vv == value:
            subdata.append(data[key1][ii])
    return subdata


# load data from file
data = load(var_names_hydro, "output_hydro.dat")

# get times
times = sorted(list(set(data["time"])))
tmax = max(times)

# loop ion variables to plot
for ivar, var in enumerate(var_names_hydro):
    if var in ["xpos", "fname", "label", "time"]:
        continue

    ymin = np.amin(data[var]) * 0.9
    ymax = np.amax(data[var]) * 1.1

    if var in log_scale:
        plot = plt.semilogy
    else:
        plot = plt.plot

    print "plotting %s" % var
    # loop on times to plot at different ages
    for itime, time in enumerate(tqdm(times)):
        plt.clf()
        tnorm = time / tmax
        xdata = subset(data, "xpos", "time", time)
        ydata = subset(data, var, "time", time)
        plot(xdata, ydata)

        # plot sound and Alfven speed
        if var in ["vx", "vy", "vz"]:
            kboltzmann = 1.38064852e-16  # erg/K
            pmass = 1.6726219e-24  # g
            gamma = 7./5.
            mu = 2e0
            cdata = np.sqrt(gamma * kboltzmann * np.array(subset(data, "Tgas", "time", time)) / mu / pmass)
            plot(xdata, cdata, ls=":", label="c_s")

            Bfield = np.array(subset(data, "|B|", "time", time))
            rho = np.array(subset(data, "rho", "time", time))
            cdata = Bfield / np.sqrt(rho * 4e0 * np.pi)
            plot(xdata, cdata, ls="--", label="v_A")
            plt.legend(loc="best")

        # compare kinetic, magnetic, and internal energy
        if var in ["E_K"]:
            cdata = subset(data, "energy", "time", time)
            plot(xdata, cdata, ls=":", label="E")
            cdata = subset(data, "E_B", "time", time)
            plot(xdata, cdata, ls="--", label="E_B")
            plt.legend(loc="best")

        fname = folder + "/plot_%s_%05d.png" % (slugify(var), itime)

        # add some ornamets
        plt.ylabel(var)
        plt.xlabel("$x$ / cm")
        plt.ylim(ymin, ymax)
        plt.tight_layout()
        plt.savefig(fname)

print "done!"
