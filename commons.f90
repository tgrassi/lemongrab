module commons
  implicit none
  !number of cells
  integer,parameter::ncell=128
  !number of non-chemistry variables
  integer,parameter::nvar=8
  !number of ghost cells
  integer,parameter::nghost=2
  !indexes of unknowns (non-chemistry)
  integer,parameter::idx_rho=1  !density
  integer,parameter::idx_rvx=2  !density*vx
  integer,parameter::idx_rvy=3  !density*vy
  integer,parameter::idx_rvz=4  !density*vz
  integer,parameter::idx_By=5   !By
  integer,parameter::idx_Bz=6   !Bz
  integer,parameter::idx_E=7    !total energy
  integer,parameter::idx_zeta=8 !cosmic-rays ionization
  !set Riemann solver order (1: constant, 2: PLM)
#ifdef PLM
  integer,parameter::order=2
#else
  integer,parameter::order=1
#endif
  integer,parameter::nall=order*(ncell+2*nghost)

  !switches
  character(len=4)::switch_resistivity_method
  logical::switch_crays, switch_isothermal, switch_chemistry
  integer::bndry, slope

  !!BEGIN_CHEMVARS

  !!END_CHEMVARS

  ! number of chemical species only (no grains)
  integer,parameter::nchem=neq-nvar-ztot

  real*8::kstore(ncell, nreacts)
  logical::isothermal_rate_stored

  !variable names
  !!BEGIN_NAMES

  !!END_NAMES

  !idx nonideal
  integer,parameter::idx_parallel=1
  integer,parameter::idx_pedersen=2
  integer,parameter::idx_hall=3
  integer,parameter::idx_ambipolar=1
  integer,parameter::idx_ohmic=2

  !max length of a reaction name
  integer,parameter::reactionNameLen=30

  !constants
  real*8,parameter::pi=4d0*atan(1d0)
  real*8,parameter::pi4=4d0*pi
  real*8,parameter::pi43=4d0*pi/3d0
  real*8,parameter::kboltzmann=1.38064852d-16 !erg/K
  real*8,parameter::pmass=1.6726219d-24 !g
  real*8,parameter::spy=3.6d3*24d0*3.65d2 !s/yr
  real*8,parameter::clight=2.99792458d10 !cm/s
  real*8,parameter::emass=9.10938356d-28 !g
  real*8,parameter::echarge=4.80320425d-10 !statC
  real*8,parameter::ev2erg=1.60218d-12 ! eV->erg
  real*8,parameter::grav=6.673d-08 ! G, cgs
  real*8,parameter::pre_kvgas_sqrt = sqrt(8d0 * kboltzmann / pi)


  !!BEGIN_OPTION_PARAMETERS

  !!END_OPTION_PARAMETERS

  !inverse of dx
  real*8,parameter::invdx=1d0/dx

  !initial conditions
  real*8::ninit(ncell, neq)

  ! cosmic ray ionization, 1/s
  !high
  real*8,parameter::cr_afit=-0.384, cr_bfit=5.34d-8
  !low
  !real*8,parameter::cr_afit=-0.211, cr_bfit=1.327d-12
  real*8,parameter::cr_N0=5d21

  integer,parameter::nang=100
  real*8::zfit_x(1000), zfit_y(1000), zfit_mult, zfit_xmin
  real*8::alpha0(nang), sin1(nang), sin2(nang), da, invjmult
  real*8::Tgas_isothermal(nall)

  !!BEGIN_MASS

  !!END_MASS


  !!BEGIN_CHARGE

  !!END_CHARGE

  !resistivity table data variables
  integer,parameter::tab_imax=16
  real*8::tab_xmin,tab_ymin,tab_zmin
  real*8::tab_invdx,tab_invdy,tab_invdz
  real*8::tab_dx,tab_dy,tab_dz
  real*8::tab_etaAD(tab_imax,tab_imax,tab_imax)

  !!BEGIN_FIT

  !!END_FIT

end module commons
