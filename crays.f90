module crays
contains

  ! *********************
  ! get cosmic rays ionization in each cell
  function get_crays(n) result(crflux)
    use commons
    implicit none
    real*8,intent(in)::n(ncell, neq)
    real*8::phi(ncell), theta(ncell), neff !(nang)
    real*8::Bfield(ncell), Bratio(ncell)
    real*8::dx_eff(ncell), acrit(ncell), dneff(ncell)
    real*8::cosa(nang), iBratio(ncell), zeta(ncell)
    real*8::lneff
    integer::i, j, jmax, jcrit(ncell)
    real*8::Bz_h(ncell), By_h(ncell), ngas_h(ncell), crflux(ncell)

    ! evaluate at the the cell interface
    do i=1,ncell-1
       By_h(i) = (n(i, idx_By) + n(i+1, idx_By)) / 2d0
       Bz_h(i) = (n(i, idx_Bz) + n(i+1, idx_Bz)) / 2d0
       ngas_h(i) = (n(i, idx_rho) + n(i+1, idx_rho)) / 2d0 / pmass / mu
    end do
    By_h(ncell) = By_h(ncell-1)
    Bz_h(ncell) = Bz_h(ncell-1)
    ngas_h(ncell) = ngas_h(ncell-1)

    ! compute Bfield component angles
    phi(:) = atan2(Bz_h(:), Bx)
    theta(:) = atan2(By_h(:), sqrt(Bz_h(:)**2 + Bx**2))

    ! effective dx travelled by guiding center (accounts for periodicity)
    dx_eff(:) = dx / cos(phi(:)) / cos(theta(:))

    ! effective column density guiding center
    dneff(:) = dx_eff(:) * ngas_h(:)

    ! initial effective column density of guiding center, cm-2
    neff = cr_N0

    ! loop on cells left to right
    do i=1, ncell
       lneff = log10(neff)
       crflux(i) = cr_bfit * neff**cr_afit !1d1**fit1d(lneff, zfit_x(:), zfit_y(:), zfit_mult, zfit_xmin)
       neff = neff + dneff(i)
    end do

  end function get_crays

  ! *******************
  ! compute the cosmic rays ionization rate time derivative
  function get_dzeta(n, drho, dBy, dBz) result(dzeta)
    use commons
    implicit none
    real*8,intent(in)::drho(ncell), dBy(ncell), n(ncell, neq), dBz(ncell)
    real*8::B(ncell), dzeta(ncell), pre, N0, dB(ncell)
    real*8::sum_nB, sum_nbbn, dn(ncell), By_h(ncell), ngas_h(ncell)
    real*8::dBy_h(ncell), dBz_h(ncell), dn_h(ncell), Bz_h(ncell)
    integer::i

    ! compute quantities at the cell interface
    do i=1,ncell-1
       dBy_h(i) = (dBy(i) + dBy(i+1)) / 2d0
       dBz_h(i) = (dBz(i) + dBz(i+1)) / 2d0
       dn_h(i) = (drho(i) + drho(i+1)) / 2d0 / pmass / mu
       By_h(i) = (n(i, idx_By) + n(i+1, idx_By)) / 2d0
       Bz_h(i) = (n(i, idx_Bz) + n(i+1, idx_Bz)) / 2d0
       ngas_h(i) = (n(i, idx_rho) + n(i+1, idx_rho)) / 2d0 / pmass / mu
    end do
    dBy_h(ncell) = dBy_h(ncell-1)
    dBz_h(ncell) = dBz_h(ncell-1)
    By_h(ncell) = By_h(ncell-1)
    Bz_h(ncell) = Bz_h(ncell-1)
    ngas_h(ncell) = ngas_h(ncell-1)
    dn_h(ncell) = drho(ncell-1)

    ! precompute some useful quantities
    pre = cr_bfit * cr_afit * dx / Bx
    B(:) = sqrt(Bx**2 + By_h(:)**2 + Bz_h(:)**2)
    dB(:) = (By_h(:) * dBy_h(:) + Bz_h(:) * dBz_h(:)) / B(:)

    ! loop over cells
    sum_nB = 0d0
    sum_nbbn = 0d0
    dzeta(1) = 0d0
    do i=1,ncell-1
       dzeta(i+1) = dzeta(i) + pre * (dx/Bx*B(i)*ngas_h(i))**(cr_afit-2) * (dn_h(i) * B(i) + dB(i) * ngas_h(i))
       ! ionization rate differential, 1/s2
       dzeta(i) = pre * (cr_N0 + dx / Bx * sum_nB)**(cr_afit-1.) * sum_nbbn
       sum_nB = sum_nB + ngas_h(i) * B(i)
       sum_nbbn = sum_nbbn + dn_h(i) * B(i) + dB(i) * ngas_h(i)
    end do

  end function get_dzeta

  ! ***************
  subroutine init_crays()
    use commons
    implicit none
    real*8,parameter::eps=pi/2d0 * 1d-2
    integer::i

    ! init initial angles
    do i=1,nang
       alpha0(i) = (i-1) * (pi / 2d0 - eps) / (nang - 1)
    end do
    da = alpha0(2) - alpha0(1)

    sin1(:) = sin(alpha0(:))
    sin2(:) = sin1(:)**2

    invjmult = 1d0 / (pi / 2d0 - eps) * (nang - 1)

    call init_fit_zeta(zfit_x(:), zfit_y(:), zfit_mult, zfit_xmin)

  end subroutine init_crays

  ! ***************
  subroutine init_fit_zeta(xdata, ydata, mult, xmin)
    implicit none
    real*8,intent(out)::xdata(:), ydata(:), mult, xmin
    real*8::ncolmin, ncolmax, ncol, xmax
    integer::i, imax

    imax = size(xdata)

    ncolmin = 1d19
    ncolmax = 1d25
    do i=1,imax
       ncol = 1d1**((i - 1) * (log10(ncolmax) - log10(ncolmin)) / (imax - 1) + log10(ncolmin))
       xdata(i) = log10(ncol)
       ydata(i) = log10(zeta_MP18_H(ncol))
    end do

    xmin = minval(xdata)
    xmax = maxval(xdata)
    mult = (size(xdata) - 1) / (xmax - xmin)

  end subroutine init_fit_zeta

  ! *******************
  function fit1d_vec(x, xdata, ydata, mult, xmin, nvec) result(f)
    implicit none
    integer,intent(in)::nvec
    real*8,intent(in)::x(nvec), xdata(:), ydata(:), mult, xmin
    real*8::f(nvec), invdx
    integer::idx(nvec), i

    invdx = 1d0 / (xdata(2) - xdata(1))

    idx(:) = (x(:) - xmin) * mult  + 1

    do i=1,nvec
       f(i) = (x(i) - xdata(idx(i))) * invdx * (ydata(idx(i)+1) - ydata(idx(i))) + ydata(idx(i))
    end do

  end function fit1d_vec

  ! *******************
  function fit1d_cr(x, xdata, ydata, mult, xmin) result(f)
    implicit none
    real*8,intent(in)::x, xdata(:), ydata(:), mult, xmin
    real*8::f, invdx
    integer::idx, i

    invdx = 1d0 / (xdata(2) - xdata(1))

    idx = (x - xmin) * mult  + 1

    f = (x - xdata(idx)) * invdx * (ydata(idx+1) - ydata(idx)) + ydata(idx)

  end function fit1d_cr

  ! ***************
  ! effective column density (cm-2) to ionization rate (1/s), HIGH
  function zeta_MP18_H(ncol_in) result(zeta)
    implicit none
    real*8,intent(in)::ncol_in
    real*8,parameter::coef(10) = (/1.001098610761d7, &
         -4.231294690194d6, &
         7.921914432011d5, &
         -8.623677095423d4, &
         6.015889127529d3, &
         -2.789238383353d2, &
         8.595814402406d0, &
         -1.698029737474d-1, &
         1.951179287567d-3, &
         -9.937499546711d-6/)
    real*8::zeta, ncol, ncol_log
    integer::i

    ncol = max(ncol_in, 1d19)
    ncol_log = log10(ncol)

    zeta = coef(1)
    do i=2,10
       zeta = zeta + coef(i)*ncol_log**(i-1)
    end do

    zeta = 1d1**zeta

  end function zeta_MP18_H

  ! ***************
  function zeta_MP18_L(ncol_in) result(zeta)
    implicit none
    real*8,intent(in)::ncol_in
    real*8,parameter::coef(10) = (/-3.331056497233d6, &
         1.207744586503d6, &
         -1.913914106234d5, &
         1.731822350618d4, &
         -9.790557206178d2, &
         3.543830893824d1, &
         -8.034869454520d-1, &
         1.048808593086d-2, &
         -6.188760100997d-5, &
         3.122820990797d-8/)
    real*8::zeta, ncol, ncol_log
    integer::i

    ncol = max(ncol_in, 1d19)
    ncol_log = log10(ncol)

    zeta = coef(1)

    do i=2,10
       zeta = zeta + coef(i)*ncol_log**(i-1)
    end do

    zeta = 1d1**zeta

  end function zeta_MP18_L

end module crays
