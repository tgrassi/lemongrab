import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import struct
from scipy.integrate import trapz
from scipy.interpolate import interp1d
from scipy import ndimage

def get_rand_field(N,n_low,n_high,k_peak,seed):

	Nhalf=int(N/2)

	kx=np.fft.fftfreq(N)*N
	kkx,kky,kkz=np.meshgrid(kx,kx,kx)
	kmag=np.sqrt(kkx**2+kky**2+kkz**2)


	spec=(kmag/k_peak)**(n_low-2)
	for i in range(len(spec)):
	 for j in range(len(spec[0])):
	  valid=kmag[i,j,:]>k_peak
	  spec[i,j,valid]=(kmag[i,j,valid]/k_peak)**(n_high-2)

	#Set the random seed
	np.random.seed(seed)

	#3 different ways of creating the random field (default is mode B)
	rmode = 'B'

	#------- Mode A ------#
	#-- Generates the real and imaginary parts as A*cos(theta) and A*sin(theta) --#
	#-- where A follows a normal distribution and theta a uniform distribution  --#
	if(rmode == 'A'):

	  sigma=np.random.standard_normal((N,N,N))*spec**0.5
	  theta=np.random.rand(N,N,N)*2*np.pi

	  sigma=sigma.T
	  theta=theta.T

	  ar=sigma*np.cos(theta)
	  ai=sigma*np.sin(theta)

	  #Zero out the 0 and the Nyquist frequencies
	  ar[0,:,:]=0.	
	  ar[:,0,:]=0.	
	  ar[:,:,0]=0.	
	  ai[0,:,:]=0.	
	  ai[:,0,:]=0.	
	  ai[:,:,0]=0.	
	  ar[Nhalf,:,:]=0.	
	  ar[:,Nhalf,:]=0.	
	  ar[:,:,Nhalf]=0.	
	  ai[Nhalf,:,:]=0.
	  ai[:,Nhalf,:]=0.	
	  ai[:,:,Nhalf]=0.	

	  #Ensures the velocity field is real -> f(k) = f*(-k)
	  ar_rev=np.flip(np.flip(np.flip(ar[1:N,1:N,1:Nhalf],2),1),0)
	  ai_rev=np.flip(np.flip(np.flip(ai[1:N,1:N,1:Nhalf],2),1),0)

	  ar[1:,1:,Nhalf+1:]=ar_rev	
	  ai[1:,1:,Nhalf+1:]=-ai_rev	

	  #Creates the f(k) complex matrix
	  phi_k=ar+ai*1j

	#------- Mode B ------#
	#-- Generates the real and imaginary parts as A+jB --#
	#-- where both A and B follow a normal distribution--#
	elif( rmode == 'B' ):

	  phi_k=np.random.standard_normal((N,N,N))+1j*np.random.standard_normal((N,N,N))
	  phi_k*=np.sqrt(spec)

	  #Zero out the 0 and the Nyquist frequencies
	  phi_k[0,:,:]=0.
	  phi_k[:,0,:]=0.
	  phi_k[:,:,0]=0.
	  phi_k[Nhalf,:,:]=0.
	  phi_k[:,Nhalf,:]=0.
	  phi_k[:,:,Nhalf]=0.
		

	  #Ensures the velocity field is real -> f(k) = f*(-k)
	  phi_k_rev=np.flip(np.flip(np.flip(phi_k[1:N,1:N,1:Nhalf],2),1),0)
	  phi_k[1:,1:,Nhalf+1:]=np.conj(phi_k_rev)

	#------- Mode C ------#
	#-- Generates a normal random field in the time domain and Fourier transform it -- #
	#-- to get a normal random field in the frequency domain (it ensures we have a   -- #
	#-- map of a real field when rescaling with the power spectrum                   -- #
	elif (rmode == 'C' ):
	  phi_r=np.random.standard_normal((N,N,N))
	  phi_k=np.fft.fftn(phi_r)*np.sqrt(spec)
	  del phi_r
	#------- Unknown mode ------#
	else:
	  print("This mode is unknown!")
	  quit()

	fx=np.fft.ifftn(phi_k)*N**3

	return np.real(fx)


def get_turbulent_velocity(N=256,n_low=10,n_high=-2,k_peak=4,seed=3089):
	if os.path.isfile('./v_%d.dat' %N) and raw_input('Generate new map (y/n)?')!='y':
	  f=open('./v_%d.dat' %N,'rb')
	  nitem='%dd' %N**3
	  vx=np.array(struct.unpack(nitem,f.read(N**3*8)))
	  vy=np.array(struct.unpack(nitem,f.read(N**3*8)))
	  vz=np.array(struct.unpack(nitem,f.read(N**3*8)))
	  vx=vx.reshape((N,N,N))
	  vy=vy.reshape((N,N,N))
	  vz=vz.reshape((N,N,N))
	  return vx,vy,vz
	vx=get_rand_field(N,n_low,n_high,k_peak,seed)
	vy=get_rand_field(N,n_low,n_high,k_peak,seed+17)
	vz=get_rand_field(N,n_low,n_high,k_peak,seed+23)

	v=vx**2+vy**2+vz**2
	v_rms=np.sqrt(np.sum(v)/len(v.flatten()))
	vx=vx/v_rms
	vy=vy/v_rms
	vz=vz/v_rms

	#Velocity found - Now we compute the energy spectrum to check
	
	ps=np.abs(np.fft.fftn(vx))**2+np.abs(np.fft.fftn(vy))**2+np.abs(np.fft.fftn(vz))**2
	ps/=N**6

	kk=np.fft.fftfreq(N)*N
	kx,ky,kz=np.meshgrid(kk,kk,kk)
	k3d=np.sqrt(kx**2+ky**2+kz**2)
	kbin=np.linspace(0,N,N)

	hh=np.histogram(k3d.flatten(),bins=kbin,weights=ps.flatten())
	fig=plt.figure()
	plt.step(0.5*(kbin[1:]+kbin[:-1]),hh[0],where='mid')

	#Theoretical spectrum
	spec=(kbin/k_peak)**n_low
	spec[kbin>k_peak]=(kbin[kbin>k_peak]/k_peak)**(n_high)

	plt.loglog(kbin,spec/np.pi,'k')
	plt.ylim(spec[2]/np.pi*0.1,np.amax(spec/np.pi)*5)
	plt.savefig('pspec.png')
	plt.clf()

	fig2=plt.figure()
	vv=v[:,:,int(N/2)]
	vv=ndimage.zoom(vv, 2, order=1)
	plt.imshow(vv/v_rms,norm=plt.matplotlib.colors.LogNorm(),cmap='Greys',origin='lower')
	plt.colorbar()
	plt.savefig('turb_map.png')
	plt.clf()

	f=open('v_%d.dat' %N,'wb')
	f.write(vx.flatten())
	f.write(vy.flatten())
	f.write(vz.flatten())
	f.close()
	return vx,vy,vz

def map_grid(Lbox,N,vec,gridx):
	dx=Lbox/(N-1)
	ixv=np.floor(vec/dx).astype(dtype='int') % N
	iyv=np.floor(np.random.random()*N).astype(dtype='int') % N
	izv=np.floor(np.random.random()*N).astype(dtype='int') % N

	return np.array(gridx[ixv,iyv,izv])

#######################################
#Get the ridge density of the filament#
#######################################
def get_rho_ridge(M_fil,L_fil,R_flat,p,R_out):
	R=np.linspace(0,R_out,10000)
	rho=1./(1+(R/R_flat)**2)**(p/2.)
	mass_to_l=trapz(rho*2*np.pi*R,R)
	return M_fil/L_fil/mass_to_l

#Constants
G=6.673e-08
Msun=1.989e33
pc=3.085678e18
AU=1.5e13
kms=1e5
mp=1.67e-24
kb=1.38e-16
#Parameters

Lbox=2.4*pc
T=15.0
res=int(sys.argv[1])
Lfil=1.6*pc
Mach=2.0
rseed=3089
Ngrid=256

#Additional parameters
M2L_fac=3.0			#Normalised mass to length of the filament
R_flat=0.033*pc			#Scale radius of the filament (perpendicular to it)
R_out=0.4*pc
p=2.
mu=2.3

cs=sqrt(kb*T/mp/mu)

M2L_crit=2*cs**2/G
print("M2L_crit: %g" %(M2L_crit/Msun*pc))

M_total=M2L_fac*M2L_crit*Lfil

rho_0=get_rho_ridge(M_total,Lfil,R_flat,p,R_out)
#Normalize Lfil
Lfil/=Lbox


x_c=np.arange(0.,1.,1./res)+0.5/res

d=np.zeros(res)
fil_filt=np.logical_and(x_c>0.5-Lfil/2.,x_c<0.5+Lfil/2.)
left_filt=x_c<=0.5-Lfil/2
right_filt=x_c>=0.5+Lfil/2
#Init density (uniform + exp decay)
d[fil_filt]=rho_0
d[left_filt]=rho_0*np.exp(-(0.5-Lfil/2-x_c[left_filt])/Lfil)
d[right_filt]=rho_0*np.exp((-x_c[right_filt]+0.5+Lfil/2.)/Lfil)


#Init Tgas
Tgas=np.ones_like(d)*T

#fion
fion=np.ones_like(d)*1e-7

#d2g
d2g=np.ones_like(d)*1e-2

#crays
crays=np.ones_like(d)*1e-17

#init velocity
v_rms=Mach*np.sqrt(kb*T/mp/mu)


vxm,vym,vzm=get_turbulent_velocity(N=Ngrid,n_low=10,n_high=-2,k_peak=6,seed=rseed)
vx=map_grid(Lbox,Ngrid,x_c*Lbox,vxm)
vy=map_grid(Lbox,Ngrid,x_c*Lbox,vym)
vz=map_grid(Lbox,Ngrid,x_c*Lbox,vzm)
print(vx,vy,vz)
#Rescale the velocity field to match the desired Mach number
vx*=v_rms
vy*=v_rms
vz*=v_rms

#Now B field
B0=0. #unless there is a perpendicular component
By=np.ones_like(d)*B0
Bz=np.ones_like(d)*B0


f=open('input.dat','w')
f.write('%d\n' %len(x_c))
for xi in x_c[:-1]: f.write('%g ' %(xi+0.5/res))
f.write('\n\n')
f.write('rho\n')
for vi in d: f.write('%g ' %vi)
f.write('\n\n')
f.write('Tgas\n')
for vi in Tgas: f.write('%g ' %vi)
f.write('\n\n')
f.write('d2g\n')
for vi in d2g: f.write('%g ' %vi)
f.write('\n\n')
f.write('crays\n')
for vi in crays: f.write('%g ' %vi)
f.write('\n\n')
f.write('fion\n')
for vi in fion: f.write('%g ' %vi)
f.write('\n\n')
f.write('vx\n')
for vi in vx: f.write('%g ' %vi)
f.write('\n\n')
f.write('vy\n')
for vi in vy: f.write('%g ' %vi)
f.write('\n\n')
f.write('vz\n')
for vi in vz: f.write('%g ' %vi)
f.write('\n\n')
f.write('By\n')
for vi in By: f.write('%g ' %vi)
f.write('\n\n')
f.write('Bz\n')
for vi in Bz: f.write('%g ' %vi)

