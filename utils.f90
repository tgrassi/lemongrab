module utils
contains

  !********************
  !load test parameters from filename, see models folder
  function load(filename, filename_chemistry) result(n)
    use commons
    use crays
    implicit none
    character(len=*),intent(in)::filename
    character(len=*),intent(in),optional::filename_chemistry
    character(20)::var, fmti, fmte, val_char
    real*8::n(ncell,neq), p(ncell), fion(ncell)
    real*8::vx1, vx2, vy1, vy2, vmod, d2g(ncell), rho_tot
    integer::unit,ios,i,j,nregions
    logical::found
    real*8,allocatable::vals(:),xpos(:)
    integer,allocatable::lcell(:),rcell(:)

    ! default values for variables
    n(:,:) = 0d0

    ! default switches
    call init_switches()

    print *, "reading from ", trim(filename)

    ! open file to read
    open(newunit=unit, file=trim(filename), status='old')

    ! integer values format
    fmti = "(a20,99I17)"

    !read number of regions
    read(unit,*) nregions
    print fmti, "regions:", nregions
    allocate(vals(nregions), xpos(nregions-1))
    allocate(lcell(nregions), rcell(nregions))
    !read interfaces positions (normalized to 1.0)
    read(unit,*) xpos(:)

    !set cell spacing
    lcell(1) = 1
    do i=1,size(vals)-1
       rcell(i) = ncell*xpos(i)
       lcell(i+1) = rcell(i) + 1
    end do
    rcell(size(vals)) = ncell

    print fmti,"lcell:", lcell
    print fmti,"rcell:", rcell

    fmte = "(a20,99E17.8e3)"
    do
       !read variable name
       read(unit, '(a)', iostat=ios) var

       !EOF
       if(ios/=0) exit

       !skip blanks
       if(trim(var)=="") cycle
       if(var(1:1)=="#") cycle

       ! use resistivity
       if(trim(var)=="resistivity" .or. trim(var)=="AD") then
          read(unit,*,iostat=ios) val_char
          switch_resistivity_method = trim(val_char)
          cycle
       end if

       !read values
       read(unit,*,iostat=ios) vals(:)

       !ngas is number density in cm3
       if(trim(var)=="ngas") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rho) = vals(i)*pmass*mu
             print fmte, "ngas->rho", vals(i), n(lcell(i),idx_rho)
          end do
          cycle
       end if

       !rho mass density, g/cm3
       if(trim(var)=="rho") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rho) = vals(i)
          end do
          cycle
       end if

       !use gas temperature to compute pressure
       if(trim(var)=="Tgas") then
          do i=1,nregions
             p(lcell(i):rcell(i)) = vals(i) * kboltzmann &
                  * n(lcell(i):rcell(i),idx_rho) / pmass / mu
             Tgas_isothermal(nghost+lcell(i):nghost+rcell(i)) = vals(i)
             print fmte, "Tgas->P", vals(i), p(lcell(i))
          end do
          cycle
       end if

       !pressure from speed of sound
       if((trim(var)=="cs") .or. (trim(var)=="csound")) then
          do i=1,nregions
             p(lcell(i):rcell(i)) = vals(i)**2 &
                  * n(lcell(i):rcell(i),idx_rho) / gamma
             print fmte,"cs->P", vals(i), p(lcell(i))
          end do
          cycle
       end if

       !if pressure found, store to compute energy below
       if(trim(var)=="p") then
          do i=1,nregions
             p(lcell(i):rcell(i)) = vals(i)
          end do
          print fmte,trim(var),vals(:)
          cycle
       end if

       !momentum from vx
       if(trim(var)=="vx") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rvx) = &
                  vals(i) * n(lcell(i):rcell(i),idx_rho)
          end do
          print fmte,trim(var),vals(:)
          cycle
       end if

       !momentum from vy
       if(trim(var)=="vy") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rvy) = &
                  vals(i) * n(lcell(i):rcell(i),idx_rho)
          end do
          print fmte, trim(var), vals(:)
          cycle
       end if

       !momentum from vz
       if(trim(var)=="vz") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rvz) = &
                  vals(i) * n(lcell(i):rcell(i),idx_rho)
          end do
          print fmte, trim(var), vals(:)
          cycle
       end if

       !ionization fraction
       if(trim(var)=="fion") then
          do i=1,nregions
             fion(lcell(i):rcell(i)) = vals(i)
          end do
          print fmte, trim(var), vals(:)
          cycle
       end if

       ! cosmic rays toggle
       if(trim(var)=="crays") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_zeta) = vals(i)
          end do
          switch_crays = .false.
          print fmte, trim(var), vals(:)
          cycle
       end if

       !dust/gas mass ratio
       if(trim(var)=="d2g") then
          do i=1,nregions
             d2g(lcell(i):rcell(i)) = vals(i)
          end do
          print fmte, trim(var), vals(:)
          cycle
       end if

       !search for variable name
       found = .false.
       !loop on var names to find match
       do i=1,neq
          if(var==varNames(i)) then
             print fmte,trim(var),vals(:)
             found = .true.
             !set left/right conditions
             do j=1,nregions
                n(lcell(j):rcell(j),i) = vals(j)
             end do
          end if
       end do
       !rise error if variable not found
       if(.not.(found)) then
          print *,"ERROR: variable not known ",trim(var)
          print *,"variables are"
          print *,varNames(:)
          stop
       end if
    end do
    close(unit)

    ! load chemistry from file, otherwise compute with fion
    if(present(filename_chemistry)) then
       call load_chemistry(filename_chemistry, n(:,:), d2g(:))
       n(:, idx_g) = d2g(:) * sum(n(:, nvar+1:nvar+nchem), 2)
    else
       ! TODO: this is hardcoded should be automatized
       !!BEGIN_INIT_X_CHEMISTRY

       !!END_INIT_X_CHEMISTRY
    end if

    !compute energy
    n(:,idx_E) = 0.5*(n(:,idx_rvx)**2 + n(:,idx_rvy)**2 &
         + n(:,idx_rvz)**2) / n(:,idx_rho) &
         + 0.5*(Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2) / pi4  &
         + p(:) / (gamma-1d0)

    ! cosmic rays using B field and density.
    ! assumes left to right propagation
    if(switch_crays) then
       n(:, idx_zeta) = get_crays(n(:,:))
    end if

    ! set isothermal temperature in the ghost cells
    ! (relevant only if switch_isothermal is on)
    Tgas_isothermal(1:nghost) = Tgas_isothermal(nghost+1)
    Tgas_isothermal(ncell+nghost+1:) = Tgas_isothermal(nghost+ncell)

    fmte = "(a20,99E17.8e3)"
    fmti = "(a20,99I10)"
    do i=1,nregions
       print *,"--------"
       print fmti,"region", i
       print fmte,"E/erg", n(lcell(i),idx_E)
       print fmte,"Tgas/K", p(lcell(i))*pmass*mu/n(lcell(i),idx_rho)/kboltzmann
       print fmte,"vx/(km/s)", n(lcell(i),idx_rvx)/n(lcell(i),idx_rho)/1d5
       print fmte,"vy/(km/s)", n(lcell(i),idx_rvy)/n(lcell(i),idx_rho)/1d5
       print fmte,"vz/(km/s)", n(lcell(i),idx_rvz)/n(lcell(i),idx_rho)/1d5
       vmod = sqrt(n(lcell(i),idx_rvx)**2 + n(lcell(i),idx_rvy)**2 + n(lcell(i),idx_rvz)**2) / n(lcell(i),idx_rho)
       print fmte,"vmod/(km/s)", vmod / 1d5
       print fmte,"cs/(km/s)", sqrt(p(lcell(i))/n(lcell(i),idx_rho)*gamma) / 1d5
       print fmte,"Mach", vmod / sqrt(p(lcell(i))/n(lcell(i),idx_rho)*gamma)
       print fmte,"vA/(km/s)", sqrt(Bx**2 + n(lcell(i),idx_By)**2 + n(lcell(i),idx_Bz)**2) / sqrt(4d0*pi*n(lcell(i), idx_rho)) / 1d5

       rho_tot = 0d0
       do j=nvar+1,nvar+nchem
          print fmte,"rho_"//trim(varNames(j))//"/(g/cm3)", n(lcell(i), j)
          rho_tot = rho_tot + n(lcell(i), j)
       end do

       do j=nvar+1,nvar+nchem
          print fmte,"n_"//trim(varNames(j))//"/(g/cm3)", n(lcell(i), j) / mass(j)
       end do

       print fmte,"rho/(g/cm3)", n(lcell(i),idx_rho)
       print fmte,"sum rho_i/(g/cm3)", rho_tot

       print fmte,"zeta CR/(s-1)", n(lcell(i), idx_zeta)
    end do

    print *, ""
    do i=1,nregions
       print *, "-------angles------- region", i
       vx1 = n(lcell(i), idx_rvx) / n(lcell(i), idx_rho)
       vy1 = n(lcell(i), idx_rvy) / n(lcell(i), idx_rho)
       print fmte, "vy/vx", atan2(vx1, vy1), atan2(vy1, vx1) * 180. / pi
       print fmte, "By/Bx", atan2(n(lcell(i), idx_By), Bx), atan2(n(lcell(i), idx_By), Bx) * 180. / pi
    end do

    !load table resistivity
    ! log10(B), log10(ngas), log10(tgas), ohmic, ambipolar, hall
    call loadResistivityTab()

    print *, "done reading!"

  end function load

  ! *******************
  ! load data from a grid of ncell points
  ! ngas/cm-3, vx/cm/s, vy/cm/s, vz/cm/s, By/G, Bz/G, Tgas/K, zeta/s-1, d2g, fion
  function load_grid(filename, filename_chemistry) result(n)
    use commons
    use crays
    implicit none
    character(len=*),intent(in)::filename
    character(len=*),intent(in),optional::filename_chemistry
    character(20)::fmte
    real*8::n(ncell,neq), ngas, vx, vy, vz, By, Bz, zeta, rho_tot(ncell)
    real*8::d2g(ncell), fion(ncell), p(ncell), Tgas(ncell), vA(ncell), vmod(ncell)
    integer::unit, i, ios, j

    ! default switches
    call init_switches()

    ! default values for variables
    n(:,:) = 0d0

    print *, "reading grid from ", trim(filename)

    ! open file to read
    open(newunit=unit, file=trim(filename), status='old')
    do i=1,ncell
       read(unit, *, iostat=ios) ngas, vx, vy, vz, By, Bz, Tgas(i), zeta, d2g(i), fion(i)
       n(i, idx_rho) = ngas * pmass * mu
       n(i, idx_rvx) = vx * n(i, idx_rho)
       n(i, idx_rvy) = vy * n(i, idx_rho)
       n(i, idx_rvz) = vz * n(i, idx_rho)
       n(i, idx_By) = By
       n(i, idx_Bz) = Bz
       p(i) = Tgas(i) * kboltzmann * n(i, idx_rho) / pmass / mu
       n(i, idx_zeta) = zeta
    end do
    close(unit)

    ! store isothermal temperature, only used if switch_isothermal is on
    Tgas_isothermal(nghost+1:nghost+ncell) = Tgas(:)
    Tgas_isothermal(1:nghost) = Tgas_isothermal(nghost+1)
    Tgas_isothermal(ncell+nghost+1:) = Tgas_isothermal(nghost+ncell)


    ! load chemistry from file, otherwise compute with fion
    if(present(filename_chemistry)) then
       call load_chemistry(filename_chemistry, n(:,:), d2g(:))
       !n(:, idx_g) = d2g(:) * sum(n(:, nvar+1:nvar+nchem), 2)
    else
       ! TODO: this is hardcoded should be automatized
              !!BEGIN_INIT_X_CHEMISTRY

       !!END_INIT_X_CHEMISTRY
    end if

    !compute energy
    n(:,idx_E) = 0.5 * (n(:, idx_rvx)**2 + n(:, idx_rvy)**2 &
         + n(:,idx_rvz)**2) / n(:,idx_rho) &
         + 0.5 * (Bx**2 + n(:, idx_By)**2 + n(:, idx_Bz)**2) / pi4  &
         + p(:) / (gamma-1d0)

    fmte = "(a20,99E17.8e3)"

    print *, "min / max"
    print fmte,"E/erg", minval(n(:,idx_E)), maxval(n(:,idx_E))
    print fmte,"Tgas/K", minval(Tgas(:)), maxval(Tgas(:))
    print fmte,"vx/(km/s)", minval(n(:,idx_rvx) / n(:,idx_rho)) / 1d5, maxval(n(:,idx_rvx) / n(:,idx_rho)) / 1d5
    print fmte,"vy/(km/s)", minval(n(:,idx_rvy) / n(:,idx_rho)) / 1d5, maxval(n(:,idx_rvy) / n(:,idx_rho)) / 1d5
    print fmte,"vz/(km/s)", minval(n(:,idx_rvz) / n(:,idx_rho)) / 1d5, maxval(n(:,idx_rvz) / n(:,idx_rho)) / 1d5
    vmod = sqrt(n(:,idx_rvx)**2 + n(:,idx_rvy)**2 + n(:,idx_rvz)**2) / n(:,idx_rho)
    print fmte,"vmod/(km/s)", minval(vmod) / 1d5, maxval(vmod) / 1d5
    print fmte,"Mach", minval(vmod / sqrt(p(:) / n(:,idx_rho) * gamma)), maxval(vmod / sqrt(p(:) / n(:,idx_rho) * gamma))
    vA(:) = sqrt(Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2) / sqrt(4d0*pi*n(:, idx_rho)) / 1d5
    print fmte,"vA/(km/s)", minval(vA), maxval(vA)
    print fmte,"Bx/G", Bx, Bx
    print fmte,"By/G", minval(n(:, idx_By)), maxval(n(:, idx_By))
    print fmte,"Bz/G", minval(n(:, idx_Bz)), maxval(n(:, idx_Bz))

    rho_tot(:) = 0d0
    do j=nvar+1,neq
       if(maxval(n(:, j))>0d0) then
          print fmte,"rho_"//trim(varNames(j))//"/(g/cm3)",  minval(n(:, j)), maxval(n(:, j))
          print fmte,"n_"//trim(varNames(j))//"/(1/cm3)",  minval(n(:, j) / mass(j)), maxval(n(:, j) / mass(j))
          rho_tot(:) = rho_tot(:) + n(:, j)
       end if
    end do

    print fmte,"sum_rho/(g/cm3)", minval(rho_tot(:)), maxval(rho_tot(:))
    print fmte,"rho/(g/cm3)", minval(n(:, idx_rho)), maxval(n(:, idx_rho))
    print fmte,"zeta CR/(s-1)", minval(n(:, idx_zeta)), maxval(n(:, idx_zeta))

  end function load_grid

  ! ***********************
  function get_timescales(n) result(ts)
    use commons
    use timescales
    implicit none
    real*8,intent(in)::n(ncell, neq)
    real*8::ts(ncell, neq)

    ts(:, :) = timescale_all(n(:, :))

  end function get_timescales

  ! **************
  subroutine save_timescale_matrix(n)
    use commons
    use timescales
    implicit none
    real*8,intent(in)::n(ncell, neq)
    real*8::ts(ncell, neq)
    integer::unit, i
    character(len=100)::fname

    fname = "timescales.dat"

    ts(:, :) = timescale_all(n(:, :))

    open(newunit=unit, file=trim(fname), status="replace")
    do i=1,ncell
      write(unit, '(999999E17.8e3)') ts(i, :)
    end do
    close(unit)

    print *, "timescale matrix saved to "//trim(fname)

  end subroutine save_timescale_matrix

  ! ***********************
  subroutine init_switches()
    use commons
    implicit none

    switch_resistivity_method = "full"
    switch_crays = .true.
    switch_isothermal = .false.
    switch_chemistry = .true.
    isothermal_rate_stored = .false.

  end subroutine init_switches

  ! *******************
  subroutine toggle_isothermal(toggle)
    use commons
    implicit none
    logical,intent(in)::toggle

    switch_isothermal = toggle

  end subroutine toggle_isothermal

  ! *******************
  subroutine toggle_chemistry(toggle)
    use commons
    implicit none
    logical,intent(in)::toggle

    switch_chemistry = toggle

  end subroutine toggle_chemistry

  ! *******************
  subroutine toggle_cosmic_rays(toggle)
    use commons
    implicit none
    logical,intent(in)::toggle

    switch_crays = toggle

  end subroutine toggle_cosmic_rays

  ! *******************
  subroutine toggle_resistivity_method(toggle)
    use commons
    implicit none
    character(len=*),intent(in)::toggle

    switch_resistivity_method = trim(toggle)

  end subroutine toggle_resistivity_method

  ! *******************
  subroutine load_chemistry(fname, n, d2g)
    use commons
    implicit none
    character(len=*),intent(in)::fname
    real*8,intent(in)::d2g(ncell)
    real*8,intent(inout)::n(ncell, neq)
    character(20)::label
    real*8::val, rhog, rhod
    integer::unit,i,ios
    logical::found

    n(:, nvar+1:neq) = 0d0

    open(newunit=unit, file=trim(fname), status="old")
    do
       read(unit, '(a)', iostat=ios) label

       !EOF
       if(ios/=0) exit

       !skip blanks
       if(trim(label)=="") cycle
       if(label(1:1)=="#") cycle
       if(label(1:1)=="!") cycle

       !read values
       read(unit,*,iostat=ios) val

       ! loop to find species with label name
       found = .false.
       do i=1,neq
          if(trim(varNames(i)) == trim(label)) then
             ! set same value in g/cm3 for all the cells
             ! read mass fractions
             n(:, i) = val * n(:, idx_rho)
             found = .true.
          end if
       end do

       ! rise error if species found in chemistry initialization file
       ! but not in the network
       if(.not.found) then
          print *, "ERROR: species "//trim(label)//" found in "//trim(fname)
          print *, " but not in the network!"
          stop
       end if

    end do
    close(unit)

    do i=1,ncell
      n(i, idx_el) = 0d0
      n(i, idx_el) = sum(n(i, nvar+1:neq) / mass(nvar+1:neq) * charge(nvar+1:neq)) * mass(idx_el)
    end do

    ! normalize to total mass
    do i=1,ncell
       rhog = sum(n(i, nvar+1:neq))
       rhod = rhog * d2g(i)
       n(i, idx_g) = rhod
       n(i, nvar+1:neq) = n(i, nvar+1:neq) / (rhog + rhod) * n(i, idx_rho)
    end do

  end subroutine load_chemistry

  !********************
  subroutine file_backup(filename)
#ifdef IFPORT
      use ifport
#endif
    implicit none
    character(len=*),intent(in)::filename
    logical::exists
    integer::err

    inquire(file=trim(filename), exist=exists)
    if(exists) then
       err = system("cp "//trim(filename)//" "//trim(filename)//".bak")
       print *, trim(filename)//" backup as "//trim(filename)//".bak"
    else
       print *, "WARNING: no backup for "//trim(filename)//" since not present"
    end if

  end subroutine file_backup

  !********************
  subroutine save_variables_to_file(n, time, unit)
    use commons
    use cooling
    use heating
    use nonideal
    use fluxes
    use crays
    use ode
#ifdef SELFGRAVITY
    use gravity
#endif
    implicit none
    integer,intent(in)::unit
    real*8,intent(in)::time, n(ncell,neq)
    real*8::ntmp(neq), ni(ncell,neq), p(ncell), xpos, Tgas(ncell)
    real*8::beta(ncell,neq), eta(ncell, 3), B2(ncell), v2(ncell)
    real*8::cool(ncell), heat(ncell), ctime(ncell), zeta(ncell)
    real*8::tmpr(nreacts), flux(ncell, nreacts), eta_tab(ncell, 3)
    real*8::ng(nall, neq), g_acc(nall), g_acc_norm(ncell)
    real*8::vx(ncell),vy(ncell),vz(ncell)
    integer::i, j, unit_init, unit_hydro, unit_beta, unit_mhd_vars

    ! temporary variables
    B2(:) = Bx**2 + n(:, idx_By)**2 + n(:, idx_Bz)**2
    vx(:) = n(:,idx_rvx) / n(:, idx_rho)
    vy(:) = n(:,idx_rvy) / n(:, idx_rho)
    vz(:) = n(:,idx_rvz) / n(:, idx_rho)
    v2(:) = vx(:)**2 + vy(:)**2 + vz(:)**2
    Tgas(:) = getTemperature(n(:,:))
    eta(:, :) = getResistivity(n(:,:), B2(:), Tgas(:), ncell)
    eta_tab(:, :) = 0d0 !getResistivityTable(n(:,:), B2(:), Tgas(:), ncell)
    cool(:) = fcool(n(:,:), Tgas(:))
    heat(:) = fheat(n(:,:), Tgas(:))
    ctime(:) = n(:, idx_E) / (cool(:) - heat(:))
    beta(:,:) = getBetaMHD(n(:,:))
    flux(:,:) = getFlux(n(:,:), Tgas(:))
    zeta(:) = get_crays(n(:,:))

    ! default acceleration value
    g_acc_norm(:) = 0d0

#ifdef SELFGRAVITY
    ni(:, :) = n(:, :)
    ng(:, :) = set_boundaries(n(:, :), ni(:, :))
    call get_g_fem(ng(:, idx_rho), g_acc(:))
    g_acc_norm(:) = g_acc(nghost+1:nghost+ncell)
#endif

    ! loop on cells to write
    do i=1,ncell
       ! temporary variables
       ntmp(:) = n(i,:)
       tmpr(:) = flux(i,:)
       write(unit, '(9999E17.8e3)') i*dx, time, Tgas(i), ntmp(1:nvar), zeta(i), &
            vx(i), vy(i), vz(i), sqrt(B2(i)), &
            eta(i, idx_ambipolar), eta(i,idx_ohmic), eta(i, idx_hall), &
            eta_tab(i, idx_ambipolar), eta_tab(i,idx_ohmic), eta_tab(i, idx_hall), &
            cool(i), heat(i), ctime(i)/spy, &
            beta(i, nvar+1:neq) * n(i, nvar+1:neq) / mass(nvar+1:neq), &
            beta(i, nvar+1:neq) * n(i, nvar+1:neq) / (1d0 + beta(i, nvar+1:neq)**2) / mass(nvar+1:neq), &
            n(i, nvar+1:neq) / (1d0 + beta(i, nvar+1:neq)**2) / mass(nvar+1:neq), &
            g_acc_norm(i), g_acc_norm(i) * n(i, idx_rho), &
            0.5 * n(i, idx_rho) * v2(i), 0.5 * B2(i) / pi4, &
            ntmp(nvar+1:neq) / ntmp(idx_rho), &
            ntmp(nvar+1:neq) / mass(nvar+1:neq), &
            sum(ntmp(nvar+1:neq) / mass(nvar+1:neq)), &
            tmpr(:)
    end do
    write(unit, *)

  end subroutine save_variables_to_file

  !******************
  subroutine save_ode(n, time, unit)
    use commons
    use ode
    implicit none
    integer,intent(in)::unit
    real*8,intent(in)::time, n(ncell,neq)
    real*8::dn(ncell,neq), ntmp(neq)
    integer::i

    dn(:,:) = fhll(n, n)
    ! loop on cells to write
    do i=1,ncell
       ! temporary variables
       ntmp(:) = dn(i,:)
       write(unit, '(99E17.8e3)') i*dx, time, ntmp(:)
    end do
    write(unit, *)

  end subroutine save_ode

  !******************
  ! compute and append MHD variables to unit file using
  ! xvar as custom variable (e.g. time)
  subroutine dumpMHDvars(n, xvar, unit)
    use commons
    use nonideal
    implicit none
    integer,intent(in)::unit
    real*8,intent(in)::n(ncell,neq),xvar
    real*8::ap,am,dn(ncell,neq)
    real*8::ng(nall,neq),irho(nall)
    real*8::dBy(nall),dBz(nall)
    real*8::By(nall),Bz(nall)
    real*8::vx(nall),vy(nall),vz(nall)
    real*8::v2(nall),B2(nall),Tgas(nall)
    real*8::Flx(nall),Fly(nall),Flz(nall)
    real*8::EADy(nall),EADz(nall),p(nall),pstar(nall)
    real*8::EADBx(nall),adf(nall),eta(nall,3)
    integer::i,j

    !copy non-ghost cell into ghost-exted structure
    ng(nghost+1:nghost+ncell,:) = n(:,:)

    !set outflow boundary conditions
    do j=1,nghost
       ng(j,:) = ng(nghost+1,:)
       ng(nghost+ncell+j,:) = ng(nghost+ncell,:)
    end do

    !derived quantities from U
    irho(:) = 1d0/ng(:,idx_rho)
    vx(:) = ng(:,idx_rvx)*irho(:)
    vy(:) = ng(:,idx_rvy)*irho(:)
    vz(:) = ng(:,idx_rvz)*irho(:)
    v2(:) = vx(:)**2 + vy(:)**2 + vz(:)**2
    By(:) = ng(:,idx_By)
    Bz(:) = ng(:,idx_Bz)
    B2(:) = (Bx**2 + ng(:,idx_By)**2 + ng(:,idx_Bz)**2)
    Tgas(:) = (gamma-1d0) * (ng(:,idx_E) - 0.5d0*ng(:,idx_rho)*v2(:) &
         - 0.5d0*B2(:)) / kboltzmann

    !dBy/dx and dBz/dx, i-centered
    do j=2,nall-1
       dBy(j) = (By(j+1)-By(j-1))*invdx*.5
       dBz(j) = (Bz(j+1)-Bz(j-1))*invdx*.5
    end do
    dBy(1) = 0d0
    dBy(nall) = 0d0
    dBz(1) = 0d0
    dBz(nall) = 0d0

    !Lorentz force, (curl B) x B
    Flx(:) = -dBy(:)*By(:)-dBz(:)*Bz(:)
    Fly(:) = dBy(:)*Bx
    Flz(:) = dBz(:)*Bx

    !EAD: ambipolar EMF, (F x B)/rho_n/rho_i/gammaAD
    !adf(:) = irho(:)**2*igAD/1d-2
    eta(:,:) = getResistivity(ng(:,:),B2(:),Tgas(:),nall)
    adf(:) = eta(:,idx_ambipolar)/B2(:)
    EADy(:) = (Flz(:)*Bx-Flx(:)*Bz(:)) * adf(:)
    EADz(:) = (Flx(:)*By(:)-Fly(:)*Bx) * adf(:)

    !EAD x B, x-component only
    EADBx(:) = EADy(:)*Bz(:)-EADz(:)*By(:)

    !pressure
    p(:) = (gamma-1d0)*(ng(:,idx_E) - 0.5*ng(:,idx_rho)*v2(:) - 0.5*B2(:))
    pstar(:) = p(:) + 0.5*B2(:)

    !open(newunit=unit,file="dataMHD.out",status='replace')
    !1:icell, 2:time, 3:adf, 4:eta, 5:B2, 6:Flz, 7:Fly, 8:EADy
    !9:EADz, 10:EADBx, 11:dBy, 12:dBz, 13:Tgas, 14:FE, 15:FBy, 16:FBz
    !17:dBy*eta, 18:1/rho/75
    do i=1,nall
       write(unit,'(I5,99E17.8e3)') i-nghost, xvar, adf(i), &
            eta(i,idx_ambipolar), &
            B2(i), Flz(i), Fly(i), &
            EADy(i), EADz(i), EADBx(i), dBy(i), dBz(i), Tgas(i), &
            (ng(i,idx_E) + pstar(i)) * vx(i) &
            - Bx*(Bx*vx(i) + ng(i,idx_By)*vy(i) + ng(i,idx_Bz)*vz(i)), &
            ng(i,idx_By)*vx(i) - Bx*vy(i), &
            ng(i,idx_Bz)*vx(i) - Bx*vz(i), &
            dBy(i)*eta(i,idx_ambipolar), irho(i)/75.
    end do
    write(unit,*)
    !close(unit)

  end subroutine dumpMHDvars

  !****************
  ! dump a table to file changing density and ionization fraction
  ! 1:Bfield, 2:ntot, 3:Tgas, 4:f_ion, 5:eta(AD)/B**2, 6:eta(AD)
  subroutine nonidealExplore()
    ! FIXME: commented
    ! use commons
    ! use nonideal
    ! implicit none
    ! integer,parameter::imax=30,jmax=30,ngrid=1
    ! integer::i,j,unit
    ! real*8::n(ngrid,neq),ngas(neq),Tgas(ngrid),B2(ngrid)
    ! real*8::ntot,fion,eta(ngrid,3),Bfield
    ! real*8::ngasMin,ngasMax,bMin,bMax,TgasMin,TgasMax,fionMin,fionMax
    !
    ! ngasMin = log10(1d0)
    ! ngasMax = log10(1d10)
    ! bMin = log10(1d-8)
    ! bMax = log10(1d0)
    ! TgasMin = log10(1d1)
    ! TgasMax = log10(1d4)
    ! fionMin = log10(1d-6)
    ! fionMax = log10(9.99999d-1)
    !
    ! fion = 1d-5
    ! Bfield = 1d-5
    !
    ! Tgas(:) = 5d1
    ! n(:,:) = 0d0
    !
    ! open(newunit=unit,file="explore.dat",status="replace")
    ! do j=1,jmax
    !    !Bfield = 1d1**((j-1)*(bMax-bMin)/(jmax-1)+bMin)
    !    !Tgas(:) = 1d1**((j-1)*(TgasMax-TgasMin)/(jmax-1)+TgasMin)
    !    fion = 1d1**((j-1)*(fionMax-fionMin)/(jmax-1)+fionMin)
    !    B2(:) = Bfield**2
    !    do i=1,imax
    !       ntot = 1d1**((i-1)*(ngasMax-ngasMin)/(imax-1)+ngasMin)
    !       n(1,idx_H2) = mass(idx_H2)*ntot*(1d0-fion)
    !       n(1,idx_el) = mass(idx_el)*ntot*fion
    !       n(1,idx_Mgj) = mass(idx_Mgj)*ntot*fion
    !       eta(:,:) = getResistivity(n(:,:), B2(:), Tgas(:), ngrid)
    !
    !       write(unit,'(99E17.8e3)') Bfield, ntot, Tgas(1), fion, &
    !            eta(1, idx_ambipolar) / Bfield**2, eta(1, idx_ambipolar)
    !    end do
    !    write(unit,*)
    ! end do
    ! close(unit)
    !
    ! print *,"nonideal explore data saved!"

  end subroutine nonidealExplore

  !******************
  function getTemperature(n) result(Tgas)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq)
    real*8::irho(ncell),vx(ncell),vy(ncell),vz(ncell)
    real*8::v2(ncell),B2(ncell),Bz(ncell),By(ncell)
    real*8::Tgas(ncell)

    if(switch_isothermal) then
       Tgas(:) = Tgas_isothermal(nghost+1:nghost+ncell)
       return
    end if

    !derived quantities from U
    irho(:) = 1d0 / n(:,idx_rho)
    vx(:) = n(:,idx_rvx)*irho(:)
    vy(:) = n(:,idx_rvy)*irho(:)
    vz(:) = n(:,idx_rvz)*irho(:)
    v2(:) = vx(:)**2 + vy(:)**2 + vz(:)**2
    By(:) = n(:,idx_By)
    Bz(:) = n(:,idx_Bz)
    B2(:) = (Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2)
    Tgas(:) = (gamma-1d0) * (n(:,idx_E) - 0.5d0 * n(:,idx_rho) * v2(:) &
         - 0.5d0 * B2(:) / pi4) / kboltzmann * irho(:) * mu * pmass

  end function getTemperature


  !*********************
  ! print first nbest fluxes using n(:,:)
  ! np(:) is an array of size nps containing selected cells for output
  ! example call with 2 sample points (ncell/4 and ncell/2) is
  ! call printFlux(n(:,:), (/ncell/4, ncell/2/), 2, 20)
  subroutine printFlux(n, np, nps, nbest)
    use commons
    use fluxes
    use rates
    implicit none
    integer,intent(in)::nps, np(nps), nbest
    real*8,intent(in)::n(ncell,neq)
    real*8::flux(ncell, nreacts), k(ncell, nreacts), Tgas(ncell)
    integer::i, j, idx(nreacts)
    character(len=reactionNameLen)::names(nreacts)

    !get temperature
    Tgas(:) = getTemperature(n(:,:))

    !get fluxes
    flux(:,:) = getFlux(n(:,:),Tgas(:))

    !get rates
    k(:, :) = getRates(n(:, :), Tgas(:))

    !get reaction names
    names(:) = getReactionNames()

    !loop on points
    do j=1,nps
       print *,"*********"
       print *,"icell:",np(j)
       print *,"Tgas/K",Tgas(np(j))
       print '(2a6,2a17,a1,a30)', "#", "idx", "rate flux", "rate coeff", " ", ""
       !sort fluxes
       idx(:) = sortf(flux(np(j),:),nreacts)
       !loop on reactions to print fluxes
       do i=1,min(nreacts, nbest)
          print '(2I6,2E17.8e3,a1,a30)', i, idx(i), flux(np(j), idx(i)), k(np(j), idx(i)), " ", names(idx(i))
       end do
    end do

  end subroutine printFlux

  !********************
  !not-so-efficient bubble sorting, returns array indexes
  function sortf(fin,nf) result(idx)
    use commons
    implicit none
    integer,intent(in)::nf
    real*8,intent(in)::fin(nf)
    real*8::rtmp,f(nf)
    integer::idx(nf),i,itmp
    logical::swap

    !local copy
    f(:) = fin(:)

    !init idx array
    do i=1,nf
       idx(i) = i
    end do

    !loop until sorted
    do
       swap = .false.
       do i=2,nf
          !check if sorted
          if(f(i-1)<f(i)) then
             !swap f if not sorted
             rtmp = f(i)
             f(i) = f(i-1)
             f(i-1) = rtmp
             !swap idx
             itmp = idx(i)
             idx(i) = idx(i-1)
             idx(i-1) = itmp
             swap = .true.
          end if
       end do
       !break when nothing more to swap
       if(.not.swap) exit
    end do

  end function sortf

  !******************
  function getBetaMHD(n) result(beta)
    use commons
    use nonideal
    implicit none
    real*8,intent(in)::n(ncell,neq)
    real*8::beta(ncell,neq),B2(ncell),B(ncell),Tgas(ncell)

    B2(:) = (Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2)
    B(:) = sqrt(B2(:))
    Tgas(:) = getTemperature(n(:,:))

    beta(:,:) = getBeta(n(:,:),B(:),Tgas(:),ncell)

  end function getBetaMHD

  !**************************
  ! load resistivity from equilibrium table, cm2/s
  ! table is x,y,z = B/G, ngas/cm-3, T/K, etaAD/cm2*s-1
  ! the code applies log
  ! table is used in nonideal.f90
  subroutine loadResistivityTab()
    use commons
    implicit none
    integer::ios,unit,ii,idx,idy,idz
    real*8::rout(6),xdata(tab_imax),ydata(tab_imax),zdata(tab_imax)

    ii = 0

    !open and check if table exists
    open(newunit=unit,file="data/resistivity_table.dat",status="old",iostat=ios)
    if(ios/=0) then
       print *,"ERROR: problem loading resistivity table!"
       stop
    end if

    !loop on file to read
    do
       read(unit,*,iostat=ios) rout(:)
       if(ios<0) exit
       if(ios>0) cycle
       idz = mod(ii,tab_imax)+1
       idy = mod(int(ii/tab_imax),tab_imax)+1
       idx = int(ii/tab_imax**2)+1

       !store AD and log10 of x,y,z data
       tab_etaAD(idx, idy, idz) = log10(rout(4))
       xdata(idx) = log10(rout(1))
       ydata(idy) = log10(rout(2))
       zdata(idz) = log10(rout(3))
       ii = ii + 1
    end do
    close(unit)

    !check if read is OK
    if(minval(xdata)==maxval(xdata)) then
       print *,"ERROR: problem reading table data!"
       stop
    end if

    !get data minimum
    tab_xmin = minval(xdata)
    tab_ymin = minval(ydata)
    tab_zmin = minval(zdata)
    !get data steps
    tab_invdx = (tab_imax-1) / (maxval(xdata) - tab_xmin)
    tab_invdy = (tab_imax-1) / (maxval(ydata) - tab_ymin)
    tab_invdz = (tab_imax-1) / (maxval(zdata) - tab_zmin)
    tab_dx = 1d0 / tab_invdx
    tab_dy = 1d0 / tab_invdy
    tab_dz = 1d0 / tab_invdz

    !print some ouput
    print *, "table range:"
    print *, "B", minval(xdata), maxval(xdata)
    print *, "n", minval(ydata), maxval(ydata)
    print *, "T", minval(zdata), maxval(zdata)

  end subroutine loadResistivityTab

end module utils
