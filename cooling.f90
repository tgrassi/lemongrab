module cooling
contains

  !*****************
  function fcool(n,Tgas)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq),Tgas(ncell)
    real*8::fcool(ncell)

    !total cooling erg/cm3/s
    fcool(:) = cool_S93(n(:,:), Tgas(:))

  end function fcool

  !******************
  !free-free cooling
  function cool_ff(n,Tgas)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq),Tgas(ncell)
    real*8::cool_ff(ncell),ffsum(ncell),ngas(ncell,neq)
    real*8,parameter::gff=1.5d0
    integer::j

    !from rhoX to number density
    do j=nvar+1,neq
       ngas(:,j) = n(:,j)*imass(j)
    end do

    !init sum of ngas*Z**2
    ffsum(:) = 0d0

    !!BEGIN_FFCOOL

    !!END_FFCOOL

    !erg/cm3/s
    cool_ff(:) = 1.4d-27*gff*sqrt(Tgas(:)) &
         * ngas(:,idx_el) * ffsum(:)

  end function cool_ff

  !*****************
  function cool_LS85(n,Tgasin) result(cool)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq),Tgasin(ncell)
    real*8::cool(ncell),Tgas(ncell)
    integer::i

    do i=1,ncell
       Tgas(i) = max(Tgasin(i),1d0)
    end do

    cool(:) = 1d1**((log10(Tgas(:))-2d0)/1.5*(29.7-26.)-29.7) &
         * n(:,idx_rho)/pmass/mu

  end function cool_LS85

  !*********************
  ! Smith+93b, https://arxiv.org/pdf/astro-ph/9703171.pdf, eqn.11
  ! erg/s
  function cool_S93(n, Tgasin) result(cool)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq), Tgasin(ncell)
    real*8::cool(ncell),Tgas(ncell)
    integer::i

    do i=1,ncell
       Tgas(i) = Tgasin(i)
       if(Tgasin(i)<1d-1) then
          Tgas(i) = 0d0 !max(Tgasin(i), 0d0)
       end if
    end do

    cool(:) = 4.2d-31 * n(:,idx_rho) / pmass / mu * Tgas(:)**3.3

  end function cool_S93

  ! ********************
  ! from KROME
  function cool_H2(n, Tgasin) result(cool)
    use commons
    implicit none
    real*8,intent(in)::n(ncell, neq), Tgasin(ncell)
    real*8::cool(ncell), log3(ncell), Tgas(ncell)
    real*8::HDL(ncell), LDL(ncell), T3(ncell)
    real*8::HDLR, HDLV, logT3(ncell), nH2(ncell), rho_H2(ncell)
    integer::i

    do i=1,ncell
       Tgas(i) = min(max(Tgasin(i), 1d1), 1d4)
    end do

    !!BEGIN_RHOH2

    !!END_RHOH2

    nH2(:) = rho_H2(ncell) / pmass / mu


    T3(:) = Tgas(:) / 1d3
    logT3(:) = log10(T3(:))

    LDL(:) = 1d1**(-2.3962112d1 +2.09433740d0*logt3(:) &
         -.77151436d0*logt3(:)**2 + .43693353d0*logt3(:)**3 &
         -.14913216D0*logt3(:)**4 - .033638326D0*logt3(:)**5) * nH2(:)

    do i=1,ncell
       if(Tgas(i) < 2d3) then
          HDLR = ((9.5e-22 * T3(i)**3.76) / (1d0 + 0.12 * T3(i)**2.1) &
               * exp(-(0.13 / T3(i))**3) + 3d-24 * exp(-0.51 / T3(i))) !erg/s
          HDLV = (6.7e-19 * exp(-5.86 / T3(i)) + 1.6d-18 * exp(-11.7 / T3(i))) !erg/s
          HDL(i)  = HDLR + HDLV !erg/s
       else
          HDL(i) = 1d1**(-2.0584225d1 + 5.0194035 * logt3(i) &
               - 1.5738805 * logt3(i)**2 - 4.7155769 * logt3(i)**3 &
               + 2.4714161 * logt3(i)**4 + 5.4710750 * logt3(i)**5 &
               - 3.9467356 * logt3(i)**6 - 2.2148338 * logt3(i)**7 &
               + 1.8161874 * logt3(i)**8) ! erg/s
       end if
    end do

    cool(:)  = nH2(:) * HDL(:) * LDL(:) / (HDL(:) + LDL(:)) !erg/cm3/s

  end function cool_H2

end module cooling
