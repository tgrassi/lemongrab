module utils_tdust
contains
  ! *****************
  ! return dust temperature
  function get_Tdust(n, Tgas) result(Tdust)
    use commons
    implicit none
    real*8,intent(in)::n(ncell, neq), Tgas(ncell)
    real*8::Tdust(ncell)

    Tdust(:) = Tgas(:)

  end function get_Tdust


  ! ************************
  function get_Tbind(idx) result(tb)
    implicit none
    integer,intent(in)::idx
    real*8::tb

    tb = 5d3  ! FIXME

  end function get_Tbind

end module utils_tdust
