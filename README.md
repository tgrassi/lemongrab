# LEMONGRAB, an MHD code to probe shock and accretion microphysics

This code is described in Grassi+2019 [https://arxiv.org/abs/1901.00504](https://arxiv.org/abs/1901.00504).      
The test in the paper refers to the commit [bc45e0f](https://bitbucket.org/tgrassi/lemongrab/commits/bc45e0f598abe046de8a4077a255d1b30b35c3aa).

The current version of the repository is different from the one described in the paper, however the main characteristics are preserved.      

Run a model with chemistry:
```
python3 main.py accretion_chemistry
make
./test
python3 plot.py
pdflatex report.tex
evince report.pdf
```

## Input details   
##### Input Grid
Grid of variable is loaded from input.dat, columns are      
ngas, vx, vy, vz, By, Bz, Tgas, zeta, d2g, fion

If chemistry is provided then ion fraction (fion) is ignored.    
If cosmic rays are computed on the fly then zeta is ignrored.    

##### New Intial conditions
Different initial conditions can be generated with scripts/make_filament_accr.py that generate a random turbulence map.      
NOTE: the number of grid cells must be the same as the variable `ncell` in commons.f90        


##### Chemistry initial conditions
Chemistry is loaded from input_chemistry.dat      
Odd lines (ignore blanks) are species variables, even lines the corresponding mass fraction scaled to the the density.     
```
H2 
1e0

C+
1e-5
```
means that H2 is set to the mass density, and C+ to 1e-5 the mass density.    


##### Chemical network
Chemical reactions are defined in the network file indicated in the models/accretion_chemistry/options.opt file.    
Dust-ions reactions are computed autmatically using the Langevin method, hence there is no need to include them in the network file.     
The possible combinations for dust-ions reactions are stored in data/dust_reactions.dat as   
```
CO+ C O
```
for example indicates that CO+ is expected to recombine with grains to form C + O as a product.       








