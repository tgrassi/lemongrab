module fluxes
contains
  ! ******************
  ! get reaction fluxes, n(:) in g/cm3
  function getFlux(n, Tgas) result(flux)
    use commons
    use rates
    implicit none
    real*8,intent(in)::n(ncell,neq),Tgas(ncell)
    real*8::flux(ncell,nreacts),k(ncell,nreacts)
    real*8::ngas(ncell,neq)
    integer::j

    !from rhoX to number density
    do j=nvar+1,neq
       ngas(:,j) = n(:,j)*imass(j)
    end do

    !get rate coefficients
    k(:,:) = getRates(n(:,:), Tgas(:))

    !!BEGIN_FLUX

    !!END_FLUX

  end function getFlux

  !************************
  function getReactionNames() result(names)
    use commons
    implicit none
    character(len=reactionNameLen)::names(nreacts)

    !!BEGIN_NAMES

    !!END_NAMES

  end function getReactionNames

end module fluxes
