module rates
contains
  ! **************
  ! precompute dust absorption rate factor f for a reactant with a given mass
  ! rate is then:
  ! R =  n_species * f
  ! since it uses total dust (i.e. charge independent)
  function k_ads(n, Tgas, idx_reactant) result(f)
    use commons
    use utils_tdust
    implicit none
    integer,intent(in)::idx_reactant
    real*8,intent(in)::Tgas(ncell)
    real*8,parameter::p3=pexp+3d0
    real*8,parameter::p4=pexp+4d0
    real*8::f(ncell), vgas(ncell), stick(ncell), n(ncell, neq), rho_dust(ncell)
    real*8::Tdust(ncell)

    ! compte Tdust (now Tgas=Tdust), K
    Tdust(:) = get_Tdust(n(:, :), Tgas(:))

    ! sticking, Hollenbach+McKee (1979)
    stick(:) = 1d0 / (1d0 + 0.04 * (Tgas(:) + Tdust(:)) + 2d-3 * Tgas(:) + 8d-6 * Tgas(:)**2)

    ! use total dust, g/cm3
    rho_dust(:) = sum(n(:, neq-ztot+1:neq), 2)

    ! vgas, cm/s
    vgas(:) = sqrt(8d0 * kboltzmann * Tgas(:) / pi / mass(idx_reactant))

    f(:) = stick(:) * pi * vgas(:) / pi43 / rho_bulk * rho_dust(:) &
         * (amax**p3 - amin**p3) / (amax**p4 - amin**p4) * p4 / p3

  end function k_ads

  ! ******************
  ! KROME wrapper
  function krate_stickSi(n, idx_reactant, Tgas) result(k)
    use commons
    use utils_tdust
    implicit none
    real*8,intent(in)::Tgas(ncell),  n(ncell, neq)
    integer,intent(in)::idx_reactant
    real*8::k(ncell)

    k(:) = k_ads(n(:, :), Tgas(:), idx_reactant)

  end function krate_stickSi

  ! **************
  ! cosmic rays desorption rate
  function k_crate_desorb(Tbind, crflux) result(k)
    use commons
    implicit none
    real*8,intent(in)::crflux(ncell), Tbind
    real*8::k(ncell)

    k(:) = 1d12 * crflux(:) / 1.3d-17 * 3.16d-19 * exp(- Tbind / 7d1)

  end function k_crate_desorb

  !***************
  ! KROME wrapper
  function krate_cr_evaporation(idx_reactant, crflux) result(k)
    use commons
    use utils_tdust
    implicit none
    real*8,intent(in)::crflux(ncell)
    real*8::k(ncell), Tbind
    integer,intent(in)::idx_reactant

    Tbind = get_Tbind(idx_reactant)

    k(:) = k_crate_desorb(Tbind, crflux(:))

  end function krate_cr_evaporation

  !***************
  ! 1D fit for rate in log space
  function fit1D(xp, xdata, ydata, xmin, fit_invdx) result(f)
    use commons
    implicit none
    real*8,intent(in)::xp(ncell),xdata(:),ydata(:)
    real*8,intent(in)::xmin,fit_invdx
    real*8::f(ncell)
    integer::i,j

    do j=1,ncell
       i = int((xp(j) - xmin) * fit_invdx) + 1
       f(j) = (xp(j) - xdata(i)) / (xdata(i+1) - xdata(i)) &
            * (ydata(i+1) - ydata(i)) + ydata(i)
    end do

  end function fit1D

  ! ****************
  function getRates(n, Tgas) result(k)
    use commons
    real*8,intent(in)::Tgas(ncell), n(ncell, neq)
    real*8::k(ncell,nreacts)

    ! store rates if isothermal and if not rates are stored already
    if(.not.isothermal_rate_stored .and. switch_isothermal) then
       kstore(:, :) = getRates_core(n(:, :), Tgas(:))
       isothermal_rate_stored = .true.
    end if

    ! use stored rates if isothermal and rates are stored already
    if(isothermal_rate_stored .and. switch_isothermal) then
       k(:, :) = kstore(:, :)
       call addRates_multi_vars(k(:, :), n(:, :), Tgas(:))
       return
    end if

    ! if not isothermal compute
    if(.not.switch_isothermal) then
       k(:, :) = getRates_core(n(:, :), Tgas(:))
       call addRates_multi_vars(k(:, :), n(:, :), Tgas(:))
       return
    end if

  end function getRates

  !******************
  ! get reaction rate coefficients
  subroutine addRates_multi_vars(k, n, Tgasin)
    use commons
    use crays
    implicit none
    real*8,intent(in)::Tgasin(ncell), n(ncell, neq)
    real*8,intent(inout)::k(ncell, nreacts)
    real*8::Tgas(ncell), crflux(ncell), ngas(ncell, neq), ntot(ncell)
    real*8::vgasH(ncell), vgasD(ncell), kadsHDa(ncell), kadsHDb(ncell)
    real*8::kadsH(ncell), kadsD(ncell), kadsHD(ncell), ngas_H(ncell), ngas_D(ncell)
    integer::i

    crflux(:) = n(:, idx_zeta)

    do i=1,ncell
       Tgas(i) = max(Tgasin(i), 1d0)
    end do

    ! g/cm3 -> 1/cm3
    do i=nvar+1,neq
       ngas(:, i) = n(:, i) * imass(i)
    end do

    ntot(:) = n(:, idx_rho) / mu / pmass

    !!BEGIN_NGAS_HD

    !!END_NGAS_HD

    vgasH(:) = pre_kvgas_sqrt * sqrt(Tgas(:) / pmass)
    vgasD(:) = vgasH(:) / sqrt(2d0)

    kadsHDa(:) = 1d0 / sqrt(2d0) / (ngas_H(:) + 1d-40)
    kadsHDb(:) = 1.6d-5 / (ngas_D(:) + 1d-40)

    kadsH(:) = 2.033d-21 * vgasH(:) * 0.5 / (ngas_H(:) + 1d-40)
    kadsD(:) = 2.033d-21 * vgasH(:) * 0.5 / (ngas_D(:) + 1d-40)
    kadsHD(:) = 2.033e-21 * vgasH(:) * (kadsHDa(:) + kadsHDb(:))

    !!BEGIN_RATES_MULTI_VARS

    !!END_RATES_MULTI_VARS

    ! DEBUG: uncomment to check rates
    do i=1,nreacts
      if(maxval(k(:, i)) > 1d0 .or. minval(k(:, i)) < 0d0) then
        print *, i, maxval(k(:, i))
      end if
    end do

  end subroutine addRates_multi_vars

  !******************
  ! get reaction rate coefficients only temperature dependent
  function getRates_core(n, Tgasin) result(k)
    use commons
    use crays
    implicit none
    integer,parameter::zminf=-4,zmaxf=4
    real*8,intent(in)::Tgasin(ncell), n(ncell, neq)
    real*8::k(ncell,nreacts),invT(ncell),sqrTgas(ncell)
    real*8::Tgas(ncell),logTgas2(ncell),logTgas(ncell)
    real*8::estick(ncell,zminf:zmaxf),stick(zminf:zmaxf,3)
    real*8::crflux(ncell), T32(ncell),sqrt_invT32(ncell)
    real*8::kpag_p_h3j(ncell), kpag_o_h3j(ncell), kpag_p_h2dj(ncell)
    real*8::kpag_o_h2dj(ncell), kpag_p_d2hj(ncell), kpag_o_d2hj(ncell)
    real*8::kpag_o_d3j(ncell), kpag_m_d3j(ncell), kpag_p_d3j(ncell)
    integer::i

    do i=1,ncell
       Tgas(i) = max(Tgasin(i), 1d0)
    end do

    ! coefficients for electron sticking, fit of Bai 2011, Fig.6 left, D=1eV
    ! array dimensions are charge, and polynomial fit coefficient index as
    ! k(Z) = 1e1**(c(Z,1)*log10(Tgas)**2 + c(Z,2)*log10(Tgas) + c(Z,3))
    stick(-4,:) = (/-0.419532959973507d0, 0.377713778369447d0, 0.0695070269849193d0/)
    stick(-3,:) = (/-0.418191116025009d0, 0.342617719784824d0, 0.184532602608894d0/)
    stick(-2,:) = (/-0.409082882254863d0, 0.3092232796856d0, 0.203395833878889d0/)
    stick(-1,:) = (/-0.392064558243327d0, 0.267302501057781d0, 0.161341043508456d0/)
    stick(0,:) = (/-0.36848365047918d0, 0.224987701201355d0, 0.0510027977105757d0/)
    stick(1,:) = (/-0.343503880962264d0, 0.214286626949166d0, -0.172001602327555d0/)
    stick(2,:) = (/-0.333851347469237d0, 0.319512474192877d0, -0.605502652636487d0/)
    stick(3,:) = (/-0.3519217024439d0, 0.553698252568829d0, -1.17743478125107d0/)
    stick(4,:) = (/-0.284044852294462d0, 0.183121835449623d0, -0.708259746433984d0/)

    !precompute derived quantities
    invT(:) = 1d0 / Tgas(:)
    sqrTgas(:) = sqrt(Tgas(:))
    logTgas(:) = log10(Tgas(:))
    logTgas2(:) = logTgas(:)**2
    T32(:) = Tgas(:) / 3d2
    sqrt_invT32(:) = sqrt(3d2 * invT)


    kpag_p_h3j = 1e1**(- 0.045543052270233676*logTgas**2 - 0.5013020450671086*logTgas - 5.692375528674977)
    kpag_o_h3j = 1e1**(+ 0.007231497108145955*logTgas**5 - 0.035640490964559755*logTgas**4 - 0.1256189916597696*logTgas**3 &
         + 0.7497804870350402*logTgas**2 - 0.9495810216706302*logTgas - 6.961445523100105)
    kpag_p_h2dj = 1e1**(- 0.0012603572315236767*logTgas**15 + 0.04182346850842126*logTgas**14 - 0.6239756030130565*logTgas**13 &
         + 5.525185329970526*logTgas**12 - 32.28123512682391*logTgas**11 + 130.84732254137774*logTgas**10 - 376.57354549794525*logTgas**9 + 774.1997352444771*logTgas**8 - 1128.594383730828*logTgas**7 + 1142.8071305678623*logTgas**6 - 775.8132068670084*logTgas**5 + 333.949138197333*logTgas**4 - 82.88128899054571*logTgas**3 + 9.73068460172044*logTgas**2 - 0.8751703858842528*logTgas - 6.13200348475948)
    kpag_o_h2dj = 1e1**(- 0.005905799229254239*logTgas**9 + 0.1192482084917681*logTgas**8 - 1.0007016650267253*logTgas**7 &
         + 4.5045742878780635*logTgas**6 - 11.688478563714296*logTgas**5 + 17.424070180489338*logTgas**4 - 14.005404410272178*logTgas**3 + 5.330518086643695*logTgas**2 - 1.2545680085745428*logTgas - 5.968563559744416)
    kpag_p_d2hj = 1e1**(- 0.005566499491766751*logTgas**10 + 0.11738276946013608*logTgas**9 - 1.0383865592353065*logTgas**8 &
         + 4.990108687413951*logTgas**7 - 14.092829565681678*logTgas**6 + 23.658499452484783*logTgas**5 - 22.94376768034369*logTgas**4 + 12.195564404395006*logTgas**3 - 3.058799576734905*logTgas**2 - 0.6133408571844539*logTgas - 6.562588683894137)
    kpag_o_d2hj = 1e1**(+ 0.00888291855256012*logTgas**7 - 0.14803248119548887*logTgas**6 + 0.9995000141008739*logTgas**5 &
         - 3.4500480696410234*logTgas**4 + 6.234001033593305*logTgas**3 - 5.403790112400253*logTgas**2 + 1.677971529728987*logTgas - 7.188427191166153)
    kpag_o_d3j = 1e1**(- 7.645021593779852e-06*logTgas**20 + 0.00026458177019026736*logTgas**19 &
         - 0.0039028505465561656*logTgas**18 + 0.03065472161982773*logTgas**17 - 0.1199793118250169*logTgas**16 &
         + 0.012823271393401578*logTgas**15 + 2.1215618181682228*logTgas**14 - 8.072416953216855*logTgas**13 &
         - 8.759657100030688*logTgas**12 + 196.63650382161111*logTgas**11 - 920.7020212397716*logTgas**10 &
         + 2565.8543147120126*logTgas**9 - 4883.825201500191*logTgas**8 + 6613.195054054789*logTgas**7 &
         - 6419.617978003055*logTgas**6 + 4414.31012182114*logTgas**5 - 2088.782225336044*logTgas**4 &
         + 649.1919621674925*logTgas**3 - 123.22381059853633*logTgas**2 + 11.837486412807664*logTgas - 6.631937227398157)
    kpag_m_d3j = 1e1**(- 0.006357983677040744*logTgas**9 + 0.1306846537018436*logTgas**8 - 1.122727954166751*logTgas**7 &
         + 5.211612650168093*logTgas**6 - 14.078801532441982*logTgas**5 + 22.14826646489963*logTgas**4 &
         - 19.211077913883216*logTgas**3 + 8.280595407852918*logTgas**2 - 2.4066872893894082*logTgas - 5.4295318491474065)
    kpag_p_d3j = 1e1**(- 0.000694691043308508*logTgas**11 + 0.01672592345692305*logTgas**10 &
         - 0.17071060999437837*logTgas**9 + 0.9498660203169156*logTgas**8 - 3.0475600089690507*logTgas**7 &
         + 5.245465059550204*logTgas**6 - 2.6788104181941454*logTgas**5 - 5.98944575687004*logTgas**4 &
         + 10.66970477377451*logTgas**3 - 5.600483083330426*logTgas**2 + 0.5567338268414032*logTgas - 6.593689760927918)


    !compute sticking from fit
    do i=zmin,zmax
       estick(:,i) = 1e1**(stick(i,1)*logTgas2(:) &
            + stick(i,2)*logTgas(:) &
            + stick(i,3))
    end do

    crflux(:) = n(:, idx_zeta)

    !estick(:,:) = 0.6

    !!BEGIN_RATES

    !!END_RATES

  end function getRates_core

end module rates
