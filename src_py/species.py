from __future__ import print_function


class Species:

    # *******************
    # constructor
    def __init__(self, string, mass_dict):
        from commons import bulkDensity, pi43, amin, amax, pexp

        # store name
        self.name = string.strip()
        self.exploded = None

        # compute charge
        self.charge = self.name.count("+") - self.name.count("-")

        neutral_name = self.name.replace("+", "").replace("-", "")
        if neutral_name == "g":
            p1 = pexp + 1e0
            p4 = pexp + 4e0
            # integrate rate and normalize to distribution integral
            self.mass = pi43 * bulkDensity * (amax**p4 - amin**p4) / (amax**p1 - amin**p1) * p1 / p4
            # removes the electron mass depending on charge
            self.mass -= self.charge * mass_dict["e-"]
            self.isDust = True
        else:
            # parse and compute mass
            self.parse(self.name, mass_dict)
            self.isDust = False

        # prepare F90-safe name
        varsafe = string.strip().replace("+", "j").replace("-", "q")
        self.isElectron = False
        if self.name.lower() in ["e", "e-"]:
            self.isElectron = True
            self.charge = -1
            varsafe = "el"
        self.fidx = "idx_" + varsafe

    # *****************
    def parse(self, species_name, mass_dict):
        import itertools
        import sys
        from utils import is_number

        species_name_arg = species_name

        # get atoms from the database sorted by name length (longer first)
        atoms = sorted(mass_dict.keys(), key=lambda x: len(x))[::-1]

        # create a list of unique signposts for atom names
        alpha = ["".join(x) for x in list(itertools.product("WYZ", repeat=4))]

        # replace atom names with slash-separated signposts
        for (i, atom) in enumerate(atoms):
            species_name = species_name.replace(atom, "/" + alpha[i] + "/")

        # replace double slashes
        while "//" in species_name:
            species_name = species_name.replace("//", "/")

        # split at slashes
        spec_list = [x for x in species_name.split("/") if x != ""]

        # search for number and when found multiply previous non-number
        #  (atom multiplicity e.g. CH2 is CHH)
        a_old = ""
        exploded = []
        for a in spec_list:
            if is_number(a):
                for j in range(int(a) - 1):
                    exploded.append(a_old)
            else:
                exploded.append(a)
            a_old = a

        # store exploded with real atom names (instead of signposts)
        try:
            exploded = [atoms[alpha.index(x)] for x in exploded]
        except KeyError:
            print("ERROR: wanted to parse ", species_name_arg)
            print(" but something went wrong with ", exploded)
            print(" Available atoms are:", [x.name for x in atoms])
            print(" Add to atom list file if needed.")
            sys.exit()

        # compute mass and assign exploded
        self.mass = sum([mass_dict[x] for x in exploded])
        # remove (add) electron mass depending on the charge
        self.mass -= self.charge * mass_dict["e-"]
        # set exploded
        self.exploded = exploded
