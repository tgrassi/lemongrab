module minmod
  use commons
contains
  !**************
  function fminmod(x,y,z) result(f)
    implicit none
    real*8,intent(in)::x(neq),y(neq),z(neq)
    real*8::f(neq)

    f(:) = 0.25 * abs(sgn(x(:))+sgn(y(:))) &
         * (sgn(x(:))+sgn(z(:))) &
         * minall(abs(x(:)),abs(y(:)),abs(z(:)))

  end function fminmod

  !**************
  function minall(x,y,z) result(f)
    implicit none
    real*8,intent(in)::x(neq),y(neq),z(neq)
    real*8::f(neq)
    integer::i

    do i=1,neq
       f(i) = minval((/x(i),y(i),z(i)/))
    end do
  end function minall

  !**************
  function sgn(x)
    implicit none
    real*8,intent(in)::x(neq)
    real*8::sgn(neq)

    sgn(:) = x(:)/abs(x(:)+1d-40)

  end function sgn

end module minmod
