module timescales
contains

  ! ************************
  function timescale_all(n) result(ts_all)
    use commons
    use ode
    implicit none
    real*8,intent(in)::n(ncell, neq)
    integer,parameter::neqa=ncell*neq
    integer::iflag, info, i
    real*8::x(neqa), fvec(neqa), tt, fjac(neqa, neqa), wa1(neqa), wa2(neqa)
    real*8::vl(neqa, neqa), vr(neqa, neqa), work(3*neqa), wr(neqa), wi(neqa), ts_all(ncell, neq)

    ! unroll
    do i=1,neq
       x(ncell*(i-1)+1:ncell*i) = n(:, i)
    end do

    iflag = 0
    tt = 0d0
    print *, "fex"
    call fex(neqa, tt, x(:), fvec(:))

    print *, "jac"
    call fdjac1(fcn, neqa, x(:), fvec(:), fjac(:, :), neqa, iflag, neqa-1, neqa-1, 0d0, wa1(:), wa2(:))

    print *, "eigen"
    call dgeev('N', 'N', neqa, fjac, neqa, wr, wi, vl, neqa, vr, neqa, work, 3*neqa, info)

    if(info /= 0) then
      print *, "ERROR: eigenvalue error"
      stop
    end if

    ! unroll eigenvalues to timescales
    do i=1,neq
       ts_all(:, i) = 1d0/(abs(wr(ncell*(i-1)+1:ncell*i)) + 1d-40)
    end do

  end function timescale_all

  !*********************
  subroutine fcn(neqa, x, fvec, iflag)
    use ode
    implicit none
    integer:: neqa, iflag
    real*8::x(neqa), fvec(neqa)
    real*8::tt

    tt = 0d0
    call fex(neqa, tt, x, fvec)

  end subroutine fcn

end module timescales
