program test
  use commons
  use utils
  use ode
  use odechem
  use nonideal, only: evaluate_eta
  implicit none
  real*8::n(ncell,neq),dt,t,tend,ntmp(neq)
  real*8::ni(ncell,neq)
  integer::unit_hydro,unit_stats
  character(len=30)::filename


  ! set slope limiter (default: 0 - MINMOD)
  slope = 1

  ! set boundary conditions (outflow:1, periodic:2)
  bndry = 2

  ! load data from input file
  n(:,:) = load_grid("input.dat", "input_chemistry.dat")

  ! store initial conditions
  ni(:,:) = n(:,:)

  filename = "output_hydro.dat"
  call file_backup(filename)

  ! open output file
  open(newunit=unit_hydro, file=filename, status="replace")

  ! dump initial conditions
  call save_variables_to_file(n(:,:), 0d0, unit_hydro)

  ! set ending time, s
  tend = 2d5*spy

  ! set timestep, s
  dt = tend / 1d1
  print '(a10,2E17.8e3)',"dt (s)",dt

  ! init total time, s
  t = 0d0

  ! loop on timesteps
  do
     print *, t/spy, t/tend

     ! do the job
     n(:,:) = fdlsodes(n(:,:), ni(:,:), dt)

     ! increase time
     t = t + dt

     ! store variables to file
     call save_variables_to_file(n(:,:), t, unit_hydro)

     ! breaks when done
     if(t>=tend) exit
  end do

  ! close output file
  close(unit_hydro)

  ! close stat file
  close(unit_stats)

  ! say goodbye
  print *,"done!"

end program test
