import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os
import glob
from utils import reaction_names, species_names


font = {'family': 'sans',
        'weight': 'normal',
        'size': 18}

matplotlib.rc('font', **font)

folder = "plots/"

if not os.path.exists(folder):
    os.makedirs(folder)

for fname in glob.glob("plots/*png"):
    os.remove(fname)

# reactions verbatim
verbs = reaction_names

var_chem = species_names
var_ngas = ["n_%s" % x for x in var_chem]

# rho*beta for MHD
betas = ["(n_%s)*(beta_%s)" % (x, x) for x in var_chem]
betas2 = ["(n_%s)*(beta_%s) / (beta_%s^2 + 1)" % (x, x, x) for x in var_chem]
betasi2 = ["(n_%s) / (beta_%s^2 + 1)" % (x, x) for x in var_chem]


# resistivity coefficients
etas = ["eAD", "eOhm", "eHall"] + ["eAD_tab", "eOhm_tab", "eHall_tab"]

# all hydro variables
var_names_hydro = ["xpos", "time", "Tgas", "rho", "rvx", "rvy", "rvz", "By", "Bz",
                   "energy", "zeta"] \
                  + ["zeta_N", "vx", "vy", "vz", "|B|"] \
                  + etas + ["cool", "heat", "ctime"] + betas + betas2 + betasi2 \
                  + ["g_acc", "g_acc x rho", "E_K", "E_B"] + var_chem + var_ngas + ["ntot"] + verbs


# sanitize filename
def slugify(value):
    import unicodedata
    import re
    value = value.replace("+", "j").replace("-", "q")
    value = str(unicodedata.normalize('NFKD', value).encode('ascii', 'ignore'))
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    value = re.sub('[-\s]+', '-', value)
    return value


# load files and store into dict, keys=variables, value=time evolution
def load(var_names, fname):
    print("loading " + fname)
    data = {x: [] for x in var_names}
    for row in open(fname):
        srow = row.strip()
        if srow == "":
            continue
        arow = [float(x) for x in srow.split(" ") if x != ""]
        if len(arow) != len(var_names):
          print("ERROR: number of variables (%d) in the file %s is not matching the expected number of variables (%d)!" %
                (len(arow), fname, len(var_names)))
        for ii, v in enumerate(var_names):
            data[v].append(arow[ii])
    data = {k: np.array(v) for k, v in data.items()}
    data["fname"] = data["label"] =\
        fname.replace(".dat", "").replace("/output_hydro", "").replace("test_", "")
    return data


# find a data subset for key1 where key2 is equal to value
def subset(data, key1, key2, value):
    subdata = []
    for ii, vv in enumerate(data[key2]):
        if vv == value:
            subdata.append(data[key1][ii])
    return np.array(subdata)


# load data from file
data = load(var_names_hydro, "output_hydro.dat")

# get times
times = sorted(list(set(data["time"])))
tmax = max(times)

# get the last time
time = times[-1]

# load colormap
cmap = plt.get_cmap("cool")

latex = "\\documentclass[11pt,a4paper]{report}\n"
latex += "\\usepackage[margin=5mm]{geometry}\n"
latex += "\\usepackage{graphicx}\n"
latex += "\\begin{document}\n"
latex += "\\noindent\n"

# loop ion variables to plot
imgcount = 0
for ivar, var in enumerate(var_names_hydro):
    if var in ["xpos", "fname", "label", "time"]:
        continue
    fname = folder + "/plot_" + slugify(var) + ".png"
    fname_zoom = folder + "/plot_zoom_" + slugify(var) + ".png"
    print("plotting %s in %s" % (var, fname))

    plt.clf()
    plt.yscale("log")
    for time in times:
        tnorm = time / tmax
        ydata = subset(data, var, "time", time)
        if min(ydata) < 0e0:
            plt.yscale("symlog", linthreshy=max(abs(ydata))*1e-4)
    plot = plt.plot

    do_plot = False
    # loop on times to plot at different ages
    for time in times:
        tnorm = time / tmax
        xdata = subset(data, "xpos", "time", time)
        ydata = subset(data, var, "time", time)

        # skip t=0 if etas to avoid crazy ylims
        if var in etas + betas + betas2 + betasi2 and time == times[0]:
            continue

        if abs(max(ydata)) > 1e-60:
            do_plot = True

        plot(xdata, ydata, color=cmap(tnorm))

        # plot sound and Alfven speed
        if var in ["vx", "vy", "vz"]:
            kboltzmann = 1.38064852e-16  # erg/K
            pmass = 1.6726219e-24  # g
            gamma = 7./5.
            mu = 2e0
            cdata = np.sqrt(gamma * kboltzmann * np.array(subset(data, "Tgas", "time", time)) / mu / pmass)
            plot(xdata, cdata, color=cmap(tnorm), ls=":", label="c_s")

            Bfield = np.array(subset(data, "|B|", "time", time))
            rho = np.array(subset(data, "rho", "time", time))
            cdata = Bfield / np.sqrt(rho * 4e0 * np.pi)
            plot(xdata, cdata, color=cmap(tnorm), ls="--", label="v_A")
            if tnorm == 0e0:
                plt.legend(loc="best")

        # compare kinetic, magnetic, and internal energy
        if var in ["E_K"]:
            cdata = subset(data, "energy", "time", time)
            plot(xdata, cdata, color=cmap(tnorm), ls=":", label="E")
            cdata = subset(data, "E_B", "time", time)
            plot(xdata, cdata, color=cmap(tnorm), ls="--", label="E_B")
            if tnorm == 0e0:
                plt.legend(loc="best")

    if not do_plot:
        continue

    # add some ornamets
    plt.title("[fig.%d] %s" % (imgcount, var), fontsize=12)
    plt.xlabel("$x$ / cm")
    plt.tight_layout()
    plt.savefig(fname)

    # latex code for including the figure
    latex += "\\includegraphics[width=0.49\\linewidth]{%s}\n" % fname.replace(".pdf", "")
    if imgcount % 2 == 1:
        latex += "\\\\\n"
    imgcount += 1


# eta_AD vs rho_var / rho plots
for var in var_ngas:
    plt.clf()
    for time in times:
        tnorm = time / tmax
        xdata = subset(data, var, "time", time) / subset(data, "ntot", "time", time)
        ydata = subset(data, "eAD", "time", time)

        plt.loglog(xdata, ydata, color=cmap(tnorm))

    plt.xlabel("$n_{%s} / n_{tot}$" % var)
    plt.ylabel("$\\eta_{AD}$")
    plt.title("[fig.%d] %s/rho vs eta_AD" % (imgcount, var), fontsize=12)
    fname = folder + slugify("/plot_eta_vs_%s.png" % var)
    plt.savefig(fname)
    latex += "\\includegraphics[width=0.49\\linewidth]{%s}\n" % fname.replace(".pdf", "")

    if imgcount % 2 == 1:
        latex += "\\\\\n"
    imgcount += 1

latex += "\\end{document}\n"

fout = open("report.tex", "w")
fout.write(latex)
fout.close()
print("all plots added in report.text, compile with")
print("pdflatex report.tex")

