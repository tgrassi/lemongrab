program test
  use commons
  use utils
  use ode
  use odechem
  use nonideal, only: evaluate_eta
  implicit none
  real*8::n(ncell,neq),dt,t,tend,ntmp(neq)
  real*8::ni(ncell,neq)
  integer::unit_hydro,unit_stats
  character(len=30)::filename

  ! precompute eta and sigma to test
  !call evaluate_eta()

  ! create equilibrium tables
  !call create_resistivity_table()

  ! ntot vs Tgas equilibrium chemistry grid
  !call chemical_grid()

  ! set slope limiter (default: 0 - MINMOD)
  slope = 1

  ! set boundary conditions (outflow:1, periodic:2)
  bndry = 1

  ! load data from input file
  n(:,:) = load_grid("input.dat")

  ! store initial conditions
  ni(:,:) = n(:,:)

  filename = "output_hydro.dat"
  call file_backup(filename)

  !open stat file
  open(newunit=unit_stats,FILE='energy.txt',status="replace")

  ! open output file
  open(newunit=unit_hydro, file=filename, status="replace")

  ! dump initial conditions
  call save_variables_to_file(n(:,:), 0d0, unit_hydro)

  ! set ending time, s
  tend = 2d5*spy

  ! set timestep, s
  dt = tend / 1d1
  print '(a10,2E17.8e3)',"dt (s)",dt

  ! init total time, s
  t = 0d0

  ! loop on timesteps
  do
     print *, t/spy, t/tend

     ! do the job
     n(:,:) = fdlsodes(n(:,:), ni(:,:), dt)

     ! increase time
     t = t + dt

     ! store variables to file
     call save_variables_to_file(n(:,:), t, unit_hydro)

     ! write stats to file
     call save_stats(t,unit_stats)

     ! breaks when done
     if(t>=tend) exit
  end do

  ! close output file
  close(unit_hydro)

  ! close stat file
  close(unit_stats)

  ! say goodbye
  print *,"done!"

contains
  subroutine save_stats(t, unit_stats)
    implicit none
    real*8::EK, ET, EB
    real*8,intent(in)::t
    integer,intent(in)::unit_stats
    EK = sum(0.5d0 * (n(:,idx_rvx)**2 + n(:,idx_rvy)**2 + n(:,idx_rvz)**2) / n(:,idx_rho))
    EB = 0.5d0 * sum(Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2) / pi4
    ET = sum(n(:,idx_E)) - EK - EB

    EK = dx**3 * EK
    ET = dx**3 * ET
    EB = dx**3 * EB
    write(unit_stats,'(5ES17.10)') t, EK, ET, EB
    flush(unit_stats)

  end subroutine save_stats

end program test
